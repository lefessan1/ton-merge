LIQUIDITY:=liquidity
DUNE_CONTRACTS_DIR:=contracts/dune-network
DUNE_TZ_CONTRACTS_DIR:=contracts/dune-network/michelson
DUNE_LOVE_CONTRACTS_DIR:=contracts/dune-network/love
LIQ_CONTRACTS = $(wildcard $(DUNE_CONTRACTS_DIR)/*.liq)
TZ_CONTRACTS = $(patsubst $(DUNE_CONTRACTS_DIR)/%.liq, $(DUNE_TZ_CONTRACTS_DIR)/%.tz, $(LIQ_CONTRACTS))
LOVE_CONTRACTS = $(patsubst $(DUNE_CONTRACTS_DIR)/%.liq, $(DUNE_LOVE_CONTRACTS_DIR)/%.lov.json, $(LIQ_CONTRACTS))
CONTRACTS_DEST:=src/blockchain/contracts
SPICED_CONTRACTS = $(patsubst $(DUNE_CONTRACTS_DIR)/%.liq, $(CONTRACTS_DEST)/contract_%.ml, $(LIQ_CONTRACTS))
SPICED_CODES = $(patsubst $(DUNE_CONTRACTS_DIR)/%.liq, $(CONTRACTS_DEST)/%_code.ml, $(LIQ_CONTRACTS))

DATABASE:=dune_ton_merge
DBVERSION=$(shell psql $(DATABASE) -c "select value from ezpg_info where name='version'" -t -A)
DBCONVERTERS=src/db/db/custom_converters

all: dune-contracts build crawler config-init deployer server openapi freeton-relay freeton-contracts

build: db-update spiced-contracts freeton-contracts
	@echo "Building ..."
	PGDATABASE=$(DATABASE) PGCUSTOM_CONVERTERS_CONFIG=$(DBCONVERTERS) dune build
	@echo "OK"

freeton-relay: _build/default/src/free-ton/relay/main.exe
	@mkdir -p bin
	@cp -fp _build/default/src/free-ton/relay/main.exe bin/freeton-relay

freeton-contracts:
	( cd contracts/free-ton ; $(MAKE) )

crawler: _build/default/src/blockchain/crawler/main.exe
	@mkdir -p bin
	@cp -fp _build/default/src/blockchain/crawler/main.exe bin/ton-merge-dune-crawler

config-init: _build/default/src/blockchain/config/init/main.exe
	@mkdir -p bin
	@cp -fp _build/default/src/blockchain/config/init/main.exe bin/ton-merge-config-init

deployer: _build/default/src/blockchain/deploy/main.exe
	@mkdir -p bin
	@cp -fp _build/default/src/blockchain/deploy/main.exe bin/ton-merge-deploy

server: _build/default/src/api/server/api_server.exe
	@mkdir -p bin
	@cp -fp _build/default/src/api/server/api_server.exe bin/ton-merge-server

# drop the db if it exists, and re-create it
init-db:
	killall ft || echo OK
	killall freeton-relay || echo OK
	psql -tc "SELECT 1 FROM pg_database WHERE datname = '$(DATABASE)'" | grep -q 1 && dropdb $(DATABASE) || echo No database
	psql -tc "SELECT 1 FROM pg_database WHERE datname = '$(DATABASE)'" | grep -q 1 || createdb $(DATABASE)

create-db: init-db

# If you don't have spice, and you didn't modify the contracts, everything
# there should be up-to-date, so let's just keep the old files
no-spice: 
	touch $(SPICED_CONTRACTS) $(SPICED_CODES)

no-liquidity:
	touch $(TZ_CONTRACTS) $(LOVE_CONTRACTS)

no-ft:
	touch contracts/free-ton/*.code

psql:
	psql $(DATABASE)

db-updater:
	PGDATABASE=$(DATABASE) dune build src/db/migrations

db-update: db-updater
	@echo "Migrating Database ..."
	_build/default/src/db/migrations/db_updater.exe --database $(DATABASE) --allow-downgrade

db-downgrade: db-updater
	_build/default/src/db/migration/db_updater.exe --allow-downgrade --database $(DATABASE) --target `expr $(DBVERSION) - 1`

openapi: doc/openapi.json
doc/openapi.json: _build/default/src/api/doc/openapi.exe
	@mkdir -p doc
	_build/default/src/api/doc/openapi.exe --version "1.0 revision $$(git describe --exclude v* --always)" --title "TON merger API" --contact "contactorigin-labs.com" --servers "api" $(API_HOST) -o doc/openapi.json

view-doc: openapi
	xdg-open 'http://localhost:28881' & redoc-cli serve -p 28881 -w doc/openapi.json

dune-contracts: $(TZ_CONTRACTS) $(LOVE_CONTRACTS)
spiced-contracts: $(SPICED_CONTRACTS) $(SPICED_CODES)

$(DUNE_TZ_CONTRACTS_DIR)/%.tz: $(DUNE_CONTRACTS_DIR)/%.liq
	@mkdir -p $(DUNE_TZ_CONTRACTS_DIR)
	@echo -n \\e[1m$(<F)\\e[0m && \
	$(LIQUIDITY) $< -o $@ 2>/tmp/tmmliqout && \
	echo " \\t\\e[92mOK\\e[0m" || echo \\e[91m ; \
	cat /tmp/tmmliqout; echo -n \\e[0m

$(DUNE_LOVE_CONTRACTS_DIR)/%.lov.json: $(DUNE_CONTRACTS_DIR)/%.liq
	@mkdir -p $(DUNE_LOVE_CONTRACTS_DIR)
	@echo -n \\e[1m$(<F)\\e[0m && \
	$(LIQUIDITY) -t Love --json --no-annot $< -o $@ 2>/tmp/tmmliqout && \
	echo " \\t\\e[92mOK\\e[0m" || echo \\e[91m ; \
	cat /tmp/tmmliqout; echo -n \\e[0m

$(CONTRACTS_DEST)/contract_%.ml $(CONTRACTS_DEST)/%_code.ml: $(DUNE_LOVE_CONTRACTS_DIR)/%.lov.json
	cd spice && spice build_ocaml --nickname $*
	cp -f spice/generated_code/ocaml/$*.0/contract_$*.ml $(CONTRACTS_DEST)/
	cp -f spice/generated_code/ocaml/$*.0/$*_code.ml $(CONTRACTS_DEST)/
	# headache -h scripts/header -c scripts/headache_config \
	# 	$(CONTRACTS_DEST)/contract_$*.ml \
	# 	$(CONTRACTS_DEST)/$*_code.ml

submodule:
	git submodule init
	git submodule update

_opam:
	opam switch create . --empty --no-install
	eval $$(opam env)

build-deps: _opam
#	opam depext .b
	opam install . --deps-only --working-dir -y

build-dev-deps: build-deps
	opam install -y merlin ocp-indent

clean:
	dune clean
