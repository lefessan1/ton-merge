
pragma ton-solidity ^0.42.0;

pragma AbiHeader expire;
pragma AbiHeader pubkey;

contract Tests {

  constructor() public
    { tvm.accept(); }

  function testSha256(
           uint256  hashed_secret, 
           string secret
           ) public pure returns
           (
           uint256 hashed1,
           uint256 hashed2,
           bool same
            )
  {
    hashed1 = hashed_secret ;
    hashed2 = sha256( secret.toSlice() );
    same = ( hashed1 == hashed2 );
  }

}

