/*
  The goal of this contract is to safeguard the list of relays/oracles that
  are allowed to sign transactions on UserSwaps.
 */

// TODO: add a budget/gas section in the architecture

pragma ton-solidity ^0.42.0;

pragma AbiHeader expire;
pragma AbiHeader pubkey;

import "./DuneUserSwap.sol";
import "./IDuneRootSwap.sol";
import "./IDuneEvents.sol";
import "./DuneSwapBase.sol";
import "./DuneRelayBase.sol";
import "./DuneConstBase.sol";


contract DuneRootSwap is IDuneRootSwap,
  DuneSwapBase, DuneRelayBase, DuneConstBase {

  struct Change {
    uint64 id ;
    uint64 creation_time ;
    uint64 ackMask ;
    uint64 rejMask ;
    uint8 n_ack ;
    uint8 n_rej ;

    uint8 nreqs ;
    uint256[] add_relays ;
    uint256[] del_relays ;
  }

  event UserSwapDeployed( uint256 pubkey, address userSwapAddr );

  modifier AuthOwnerOrRelay() {
    if ( msg.pubkey() != tvm.pubkey() ) _isRelay( msg.pubkey() );
    _;
  }




  /* VARIABLES */


  uint8 public g_nrelays ;       // number of relays
  uint8 public g_relay_counter ;
  uint64 public g_expiration_date ;

  uint64 g_change_counter ;
  mapping(uint64 => Change) g_changes;

  address public g_event_address = address(0) ;
  uint256 g_prev_user_key ;





  
  // @param relays The initial set of relays/oracles public keys
  // @param nreqs The initial minimal number of confirmation for a swap order
  // @param duneUserSwapCode The code of the DuneUserSwap contract to be able
  //        to deploy user contracts
  // @param giver_address The address of the DuneGiver contract
  // @param expiration_date The date after which no new user contract
  //        can be created
  constructor(
              uint256[] relays,
              uint8 nreqs,
              TvmCell duneUserSwapCode,
              address giver_address,
              uint64 expiration_date
              ) AuthOwner() public
    {
      require( relays.length > 0 && relays.length <= MAX_RELAYS,
               EXN_BAD_NUMBER_OF_RELAYS );
      tvm.accept();

      g_expiration_date = expiration_date + SWAP_EXPIRATION_TIME ;

      uint256 len = relays.length;
      for ( uint8 i = 0 ; i < len ; i++ ){
        _addRelay( relays [i] );
      }
      require( nreqs > 0 && nreqs <= g_nrelays,
               EXN_BAD_NUMBER_OF_CONFIRMATIONS );
      g_nreqs = nreqs ;

      g_duneUserSwapCode = duneUserSwapCode ;
      g_giver_address = giver_address ;
      g_root_address = address( this );
    }





  function cleanChanges( ) public {
    _isRelay( msg.pubkey() ) ;
    tvm.accept();

    optional(Change) opt_change = _getChange( g_change_counter );
    require( ! opt_change.hasValue() , EXN_CHANGE_NOT_EXPIRED );
  }




  function proposeChange( uint8 nreqs, uint256[] add_relays, uint256[] del_relays)
    public returns ( uint64 changeId ) {
    uint8 index = _isRelay( msg.pubkey() ) ;
    require( uint64(now) < g_expiration_date, EXN_SWAP_EXPIRED );
    tvm.accept();

    optional(Change) opt_prev = _getChange( g_change_counter );
    require ( !opt_prev.hasValue() , EXN_CHANGE_ALREADY_PROPOSED );

    g_change_counter++ ;
    changeId = g_change_counter ;
    Change change = Change(
                           changeId,
                           now, // creation_time
                           0, // ackMask
                           0, // rejMask
                           0, // n_ack
                           0, // n_rej
                           nreqs,
                           add_relays,
                           del_relays
                           );
    g_changes[ changeId ] = change ;

    _acceptChange( change, index );
  }



  function acceptChange( uint64 changeId ) public {
    require( changeId > 0 && changeId == g_change_counter,
             EXN_WRONG_CHANGE_ID );
    uint8 index = _isRelay( msg.pubkey() ) ;
    tvm.accept();

    optional(Change) opt_change = _getChange( changeId );
    _acceptChange( opt_change.get(), index );
  }




  function rejectChange( uint64 changeId ) public {
    require( changeId > 0 && changeId == g_change_counter,
             EXN_WRONG_CHANGE_ID );
    uint8 index = _isRelay( msg.pubkey() ) ;
    tvm.accept();

    optional(Change) opt_change = _getChange( changeId );
    _rejectChange( opt_change.get(), index );
  }





  function _addRelay( uint256 key ) private {

    if( ! g_relays.exists(key) ){
      require( g_relay_counter < MAX_RELAYS, EXN_TOO_MANY_RELAYS );
      g_relays[key] = g_relay_counter++ ;
      g_nrelays++ ;
    }

  }

  function _delRelay( uint256 key ) private {

    if( g_relays.exists(key) ){
      delete g_relays[key];
      g_nrelays-- ;
    }
  }

  function _isRelay( uint256 key ) private view returns ( uint8 index ) {
    index = _isRelayExn(key, EXN_NO_RELAY_FOR_PUBKEY);
  }



  function _getChange( uint64 changeId ) private
    returns ( optional(Change) opt_change ){

    opt_change = g_changes.fetch ( changeId );
    if ( opt_change.hasValue() ){
      Change change = opt_change.get() ;
      if( uint64(now) - change.creation_time > CHANGE_EXPIRATION_TIME ){
        opt_change.reset();
        delete g_changes[ changeId ];
      }
    }
  }



  function _acceptChange( Change change, uint8 index ) private {

    uint64 bit = uint64(1) << index ;
    require ( ( change.ackMask & bit ) == 0, EXN_ALREADY_VOTED_FOR_CHANGE );
    change.ackMask |= bit ;
    change.n_ack++;

    if( change.n_ack >= g_nreqs ){
      delete g_changes[ change.id ];
      g_nreqs = change.nreqs ;

      uint256[] relays = change.del_relays;
      uint256 len = relays.length;
      for ( uint8 i = 0 ; i < len ; i++ ){
        _delRelay( relays [i] );
      }
      relays = change.add_relays;
      len = relays.length;
      for ( uint8 i = 0 ; i < len ; i++ ){
        _addRelay( relays [i] );
      }
      require ( g_nrelays > 0 , EXN_BAD_NUMBER_OF_RELAYS );
      require ( g_nreqs > 0 && g_nreqs <= g_nrelays,
                EXN_BAD_NUMBER_OF_CONFIRMATIONS );

      delete g_changes[ change.id ];
    } else {
      g_changes [ change.id ] = change ;
    }
  }



  function _rejectChange( Change change, uint8 index ) private {

    uint64 bit = uint64(1) << index ;
    require ( ( change.rejMask & bit ) == 0, EXN_ALREADY_VOTED_FOR_CHANGE );
    change.rejMask |= bit ;
    change.n_rej++;

    if( change.n_rej >= g_nreqs ){
      delete g_changes[ change.id ];
    } else {
      g_changes [ change.id ] = change ;
    }
  }





  function getState() public view returns
    (
     uint8 nreqs,
     uint256[] relays,
     uint8[] indexes
     )
  {
    nreqs = g_nreqs;
    optional(uint256,uint8) opt_relay = g_relays.min() ;
    while( opt_relay.hasValue() ){
      (uint256 relay, uint8 index) = opt_relay.get();
      relays.push( relay );
      indexes.push( index );
      opt_relay = g_relays.next(relay);
    }
  }




  function getChange() public view returns (Change change)
  {
    optional(Change) opt_change = g_changes.fetch ( g_change_counter );
    change = opt_change.get();
  }




  function deployUserSwap( uint256 pubkey ) public
    returns ( address newSwap )
  {
    require( g_prev_user_key != pubkey, EXN_ALREADY_DEPLOYED );
    _isRelay( msg.pubkey() ) ;
    require( uint64(now) < g_expiration_date, EXN_SWAP_EXPIRED );
    tvm.accept();

    (uint8 nreqs, uint256[] relays, uint8[] indexes) = getState();

    newSwap = new DuneUserSwap
      {
      code: g_duneUserSwapCode ,
      value: 1 ton ,
      pubkey: pubkey ,
      varInit: {
                s_giver_address : g_giver_address ,
                s_root_address: g_root_address
      }
      }( g_expiration_date, nreqs, relays, indexes, g_event_address ) ;

    g_prev_user_key = pubkey ;
    emit UserSwapDeployed( pubkey, newSwap );
  }




  function closeSwap() public AuthOwnerOrRelay() {
    require( uint64(now) > g_expiration_date, EXN_SWAP_NOT_EXPIRED );
    tvm.accept();
    selfdestruct( g_giver_address );
  }




  function updateRelays( uint256 user_pubkey ) public view AuthOwnerOrRelay() {
    tvm.accept();

    address computed_addr = _getUserSwapAddress( user_pubkey );

    (uint8 nreqs, uint256[] relays, uint8[] indexes) = getState();
    IDuneUserSwap( computed_addr ). updateRelays( relays, indexes, nreqs );
  }

  function getSwapInfo ( address event_address ) public override AuthGiver() {
    g_event_address = event_address ;
    // flag:1 means that we pay fees from our balance, not from the value
    IDuneGiver( g_giver_address ).setSwapInfo
      { flag: 1 }( g_duneUserSwapCode, g_expiration_date );
  }


  function get() public view returns
    (
     uint8 nreqs,
     uint256[] relays,
     uint8[] indexes,
     address root_address,
     address giver_address,
     address event_address,
     uint8 nrelays,
     uint8 relay_counter,
     uint64 expiration_date,
     uint64 change_counter,
     uint8 change_state,
     uint256 prev_user_key
     )
  {
    nreqs = g_nreqs;
    optional(uint256,uint8) opt_relay = g_relays.min() ;
    while( opt_relay.hasValue() ){
      (uint256 relay, uint8 index) = opt_relay.get();
      relays.push( relay );
      indexes.push( index );
      opt_relay = g_relays.next(relay);
    }
    root_address = g_root_address ;
    giver_address = g_giver_address ;
    event_address = g_event_address ;
    
    nrelays = g_nrelays;
    relay_counter = g_relay_counter ;
    expiration_date = g_expiration_date ;
    change_counter = g_change_counter ;
    prev_user_key = g_prev_user_key ;

    optional(Change) opt_change = g_changes.fetch ( change_counter );
    if ( opt_change.hasValue() ){
      Change change = opt_change.get() ;
      if( uint64(now) - change.creation_time > CHANGE_EXPIRATION_TIME ){
        change_state = 2 ; // change expired
      } else {
        change_state = 1 ; // change waiting
      }
    } else {
      change_state = 0 ; // no change waiting
    }

  }
  
}

