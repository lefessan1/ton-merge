pragma ton-solidity ^0.42.0;

pragma AbiHeader expire;
pragma AbiHeader pubkey;

import "./DuneUserSwap.sol";

abstract contract DuneSwapBase {

  uint8 constant EXN_AUTH_FAILED = 100 ;

  // address of the DuneRootSwap contract
  address public g_root_address ;
  // address of this contract
  address public g_giver_address ;
  // code of DuneUserSwap contract to compute and verify addresses
  TvmCell public g_duneUserSwapCode ;
  
  modifier AuthOwner() {
    require( msg.pubkey() == tvm.pubkey(), EXN_AUTH_FAILED );
    _;
  }

  modifier AuthRoot() {
    require( msg.sender == g_root_address, EXN_AUTH_FAILED );
    _;
  }

  modifier AuthGiver() {
    require( msg.sender == g_giver_address, EXN_AUTH_FAILED );
    _;
  }

  modifier AuthUser(uint256 pubkey) {
    address user_addr = _getUserSwapAddress( pubkey );
    require( msg.sender == user_addr, EXN_AUTH_FAILED );
    _;
  }

  function _getUserSwapAddress( uint256 pubkey )
    internal view returns ( address addr )
  {
    /* Recompute the address that a user with the provided pubkey
       should have, if he uses the same contract and static
       variables. */
    TvmCell stateInit = tvm.buildStateInit({
      contr: DuneUserSwap,
          pubkey: pubkey,
          code: g_duneUserSwapCode,
          varInit: {
             s_giver_address : g_giver_address ,
             s_root_address: g_root_address
            }
          });
    addr = address(tvm.hash(stateInit));
  }

  function getUserSwapAddress( uint256 pubkey )
    public view returns ( address addr )
  {
    addr = _getUserSwapAddress( pubkey );
  }

}
