/*
  Implementation of contract DuneGiver
 */

pragma ton-solidity ^0.42.0;

pragma AbiHeader expire;
pragma AbiHeader pubkey;

import "./IDuneGiver.sol";
import "./IDuneRootSwap.sol";
import "./IDuneEvents.sol";
import "./DuneSwapBase.sol";

contract DuneGiver is IDuneGiver, DuneSwapBase {

  uint8 constant EXN_MISSING_PUBKEY = 101 ;
  uint8 constant EXN_INIT_TWICE = 102 ;
  uint8 constant EXN_NOT_INITIALIZED = 103 ;
  uint8 constant EXN_SWAP_EXPIRED = 104 ;
  uint8 constant EXN_SWAP_NOT_EXPIRED = 105 ;
  uint8 constant EXN_BALANCE_TOO_LOW = 106 ;
  
  // address of the DuneEvents contract
  address public g_event_address ;
  // expiration date after which no transfer is possible
  uint64 public g_expiration_date ;
  // address of the main FreeTON giver
  address public g_freeton_giver ;


  event OrderCredited ( string order_id, uint256 pubkey, uint128 ton_amount );



  modifier OwnerSet() {
    require( tvm.pubkey() != 0, EXN_MISSING_PUBKEY );
    _;
  }

  modifier RootSet() {
    require( g_root_address != address(0), EXN_NOT_INITIALIZED );
    _;
  }


  // @param freeton_giver The official FreeTON giver, so that we can send
  //     back unused tokens after the expiration date
  constructor( address freeton_giver ) public OwnerSet() AuthOwner() {
    tvm.accept();

    g_freeton_giver = freeton_giver ;
  }



  /* Initialize the giver with the address of the DuneRootSwap contract
     used to deploy DuneUserSwap contracts. Also, the expiration date
     can be used to prevent any transfer after a given time.
  */
  function init( address root_address, address event_address ) public
    AuthOwner()
  {
    require( g_root_address == address(0) , EXN_INIT_TWICE );
    require( address(this).balance > 20000 ton , EXN_BALANCE_TOO_LOW );
    tvm.accept();

    
    g_root_address = root_address ;
    g_giver_address = address(this) ;
    g_event_address = event_address ;

    IDuneRootSwap( root_address ).getSwapInfo
      { value: 20000 ton }( event_address );
  }



  // @dev Set the userSwapCode and expiration date sent by the
  //      DuneRootSwap contract as a reply to getSwapInfo()
  // @param userSwapCode The code of the DuneUserSwap to be able to verify
  //        addresses
  // @param expiration_date The date at which no more transfers are allowed
  function setSwapInfo(TvmCell duneUserSwapCode, uint64 expiration_date)
    public override RootSet() AuthRoot()
  {
    g_expiration_date = expiration_date ;
    g_duneUserSwapCode = duneUserSwapCode ;
    IDuneEvents( g_event_address ).init
      { flag: 1 }( g_root_address, duneUserSwapCode, expiration_date );
  }



  // @dev Transfer TON tokens to a DuneUserSwap contract
  // @param credit_id Unique identifier of the swap
  // @param ton_amount Amount of nanoton to transfer back
  // @param pubkey Public key associated with the swap user
  function creditOrder(
                         string order_id,
                         uint128 ton_amount,
                         uint256 pubkey) public override RootSet()
  {
    require( uint64(now) < g_expiration_date, EXN_SWAP_EXPIRED );
    tvm.accept();

    bool credited = false ;
    
    address computed_addr = _getUserSwapAddress( pubkey );

    // Fail if the caller is not the DuneUserSwap associated with this
    // pubkey.
    require( msg.sender == computed_addr, EXN_AUTH_FAILED );

    if( address(this).balance >= ton_amount ){

      // Otherwise, send back the requested tokens
      // TODO: it may fail if we don't have enough balance
      IDuneUserSwap( computed_addr ).receiveCredit
        { value: ton_amount + ( 1 ton ), bounce: true, flag: 0 }
      ( order_id );

      emit OrderCredited ( order_id, pubkey, ton_amount );
      credited = true ;
    } else {
      IDuneUserSwap( msg.sender ).creditDenied( order_id );
    }
    IDuneEvents( g_event_address ).orderCredited ( order_id, credited );
  }




  // @dev Send back unused tokens to the main giver when expiration
  // date is passed.
  // @caller admin with same pubkey
  function closeSwap() public AuthOwner() {
    tvm.accept();

    require( uint64(now) > g_expiration_date , EXN_SWAP_NOT_EXPIRED ) ;

    selfdestruct( g_freeton_giver );
  }




  fallback () external {}

  function get() public view returns(
                                address root_address,
                                address giver_address,
                                address event_address,
                                uint64 expiration_date,
                                address freeton_giver
  ) {
    root_address = g_root_address ;
    giver_address = g_giver_address ;
    event_address = g_event_address ;
    expiration_date = g_expiration_date ;
    freeton_giver = g_freeton_giver ;
  }

}


