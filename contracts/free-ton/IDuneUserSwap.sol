/* Interface of a User Swap Contract */

pragma ton-solidity ^0.42.0;

interface IDuneUserSwap {

  function receiveCredit( string order_id ) external ;
  function creditDenied( string order_id ) external ;
  
  function updateRelays( uint256[] relays,
                         uint8[] indexes,
                         uint8 nreqs) external;
}
