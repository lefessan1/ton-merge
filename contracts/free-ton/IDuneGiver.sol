/* Interface IDuneGiver */

pragma ton-solidity ^0.42.0;

interface IDuneGiver {

  function creditOrder(
                         string credit_id,
                         uint128 ton_amount,
                         uint256 pubkey
                         ) external ;
  
  function setSwapInfo(
                       TvmCell walletCode,
                       uint64 expiration_date
                       ) external ;
}
