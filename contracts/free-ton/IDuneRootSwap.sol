/* Interface IRootUserWallet */

pragma ton-solidity ^0.42.0;

interface IDuneRootSwap {

  function getSwapInfo( address event_address ) external ;
}
