pragma ton-solidity ^0.42.0;

pragma AbiHeader expire;
pragma AbiHeader pubkey;

abstract contract DuneConstBase {

  // uint8 constant EXN_AUTH_FAILED = 100 ;
  uint8 constant EXN_BAD_NUMBER_OF_RELAYS = 101 ;
  uint8 constant EXN_SWAP_EXPIRED = 102 ;
  uint8 constant EXN_CHANGE_ALREADY_PROPOSED = 103 ;
  uint8 constant EXN_WRONG_CHANGE_ID = 104 ;
  uint8 constant EXN_TOO_MANY_RELAYS = 105 ;
  uint8 constant EXN_NO_RELAY_FOR_PUBKEY = 106 ;
  uint8 constant EXN_ALREADY_VOTED_FOR_CHANGE = 107 ;
  uint8 constant EXN_BAD_NUMBER_OF_CONFIRMATIONS = 108 ;
  uint8 constant EXN_SWAP_NOT_EXPIRED = 109 ;
  uint8 constant EXN_CHANGE_NOT_EXPIRED = 110 ;
  uint8 constant EXN_ALREADY_DEPLOYED = 111 ;
  uint8 constant EXN_UNEXPECTED_ANSWER_FROM_DEPOOL = 112 ;
  uint8 constant EXN_FORMER_DEPOOL_ORDER_NOT_FINISHED = 113 ;
  uint8 constant EXN_ALREADY_CONFIRMED = 114 ;
  uint8 constant EXN_UNKNOWN_ORDER = 115 ;
  uint8 constant EXN_NOT_YET_CREDITED = 116 ;
  uint8 constant EXN_ORDER_EXPIRED = 117 ;
  uint8 constant EXN_WRONG_SECRET = 118 ;
  uint8 constant EXN_NOT_CANCELLABLE = 119 ;
  uint8 constant EXN_NOT_YET_EXPIRED = 120 ;
  uint8 constant EXN_NOT_YET_REVEALED = 121 ;

  uint64 constant DAY_SECONDS = 86400 ;
  uint64 constant WEEK_SECONDS = DAY_SECONDS * 7 ;
  uint64 constant MONTH_SECONDS = DAY_SECONDS * 30;

  uint64 constant SWAP_EXPIRATION_TIME = WEEK_SECONDS;
  uint64 constant CHANGE_EXPIRATION_TIME = DAY_SECONDS ;
  uint64 constant DEPOOL_FEE = 500000000 ;
  uint64 constant MAX_RELAYS = 64 ;

  uint8 constant STATE_WAITING_FOR_CONFIRMATION = 0 ;
  uint8 constant STATE_FULLY_CONFIRMED          = 1 ;
  uint8 constant STATE_WAITING_FOR_CREDIT       = 2 ;
  uint8 constant STATE_CREDIT_DENIED            = 3 ;
  uint8 constant STATE_CREDITED                 = 4 ;
  uint8 constant STATE_REVEALED                 = 5 ;
  uint8 constant STATE_WAITING_FOR_DEPOOL       = 6 ;
  uint8 constant STATE_DEPOOL_DENIED            = 7 ;
  uint8 constant STATE_TRANSFERRED              = 8 ;
  uint8 constant STATE_CANCELLED                = 9 ;

}
