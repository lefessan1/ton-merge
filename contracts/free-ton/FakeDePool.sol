/*
  Implementation of contract DePool: a fake depool to be able to test...
 */

pragma ton-solidity ^0.42.0;

pragma AbiHeader expire;
pragma AbiHeader pubkey;

import "./IDePool.sol";
import "./IParticipant.sol";

contract FakeDePool is IDePool {

  uint8 constant EXN_AUTH_FAILED = 100 ;
  uint8 constant EXN_NO_SENDER = 101 ;


  uint64 constant DEPOOL_FEE = 500000000 ;


  address public g_donor ;
  address public g_participant ;
  uint32 public n_errors ;

  constructor() public
    {
      require( tvm.pubkey() == msg.pubkey() , EXN_AUTH_FAILED );
      tvm.accept();
    }


  // @dev Vest some stake for benefiary. Calls back the sender
  //    receiveAnswer method in case of success, or error (returning
  //    the stake) The benefiary must have declared the sender as a
  //    donor beforehand, using the setVestingDonor() method. ALso, a
  //    fee of 0.5 TON is expected
  // @param stake The total skate in nanoton
  // @param benefiary The address that should received unlocked tokens
  // @param withdrawalPeriod The time between incremental unvesting
  // @param totalPeriod      The total time of the vesting
  function addVestingStake(  uint64  stake
                           , address beneficiary
                           , uint32   // withdrawalPeriod
                           , uint32   // totalPeriod
                           )
    public override
  {

    require( msg.sender != address(0), EXN_NO_SENDER );

    if(
       beneficiary != g_participant
       || msg.sender != g_donor
       || msg.value < stake + DEPOOL_FEE
      ) {
      n_errors++ ;

      IParticipant( g_donor ).receiveAnswer
        { value: msg.value, bounce: false, flag : 64 } ( 1, 0 );
    } else {
      IParticipant( g_donor ).receiveAnswer
        { value: DEPOOL_FEE - 0.1 ton, bounce: false } ( 0, 0 );

    }

  }

  // @dev Declare a new donor for the sender, prior to the use of
  //    addVestingStake()
  // @param donor The address of the donor
  function setVestingDonor(address donor) public {
    require( msg.sender != address(0), EXN_NO_SENDER );

    g_donor = donor ;
    g_participant = msg.sender ;
  }

}

