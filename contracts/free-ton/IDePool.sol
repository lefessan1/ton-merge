/* Interface IDePool */

pragma ton-solidity ^0.42.0;

interface IDePool {
  
  function addVestingStake(uint64 stake, address beneficiary, uint32 withdrawalPeriod, uint32 totalPeriod) external ;

}
