pragma ton-solidity ^0.42.0;

pragma AbiHeader expire;
pragma AbiHeader pubkey;

import "./IDuneUserSwap.sol";
import "./IDuneGiver.sol";
import "./IDePool.sol";
import "./IParticipant.sol";
import "./IDuneEvents.sol";
import "./DuneRelayBase.sol";
import "./DuneConstBase.sol";


/* We may create a specific contract DuneEventContract to be able
   to monitor everything while observing only one contract */

contract DuneUserSwap is IDuneUserSwap, IParticipant,
  DuneRelayBase, DuneConstBase
{
  uint8 constant EXN_AUTH_FAILED = 100 ;

  struct SwapOrder {
    string order_id ;
    uint256 hashed_secret ;
    uint64 ton_amount ;
    uint64 dun_amount ;
    address dest ;
    address depool ; // can be address(0)
    uint8 state ;
    uint32 state_count ;
    uint8 nreqs ;
    uint64 voteMask ;
    uint64 expiration_date ;
  }


  event OrderStateChanged ( string order_id, uint8 state );
  event OrderRevealed ( string order_id, string secret );



  address static s_giver_address ;  // DuneGiver
  address static s_root_address ;   // DuneRootSwap
  address public g_event_address ;
  uint64 public g_expiration_date ; // nothing can happen after this
  mapping ( string => SwapOrder ) g_orders;
  optional(string) g_depool_order;




  modifier AuthOwner() {
    require( msg.pubkey() == tvm.pubkey(), EXN_AUTH_FAILED );
    _;
  }

  modifier AuthOwnerOrRelay() {
    if ( msg.pubkey() != tvm.pubkey() ) _isRelay( msg.pubkey() );
    _;
  }

  modifier AuthRoot() {
    require( msg.sender == s_root_address, EXN_AUTH_FAILED );
    _;
  }

  modifier AuthGiver() {
    require( msg.sender == s_giver_address, EXN_AUTH_FAILED );
    _;
  }




  constructor(uint64 expiration_date,
              uint8 nreqs,
              uint256[] relays,
              uint8[] indexes,
              address event_address ) public AuthRoot()
    {
      tvm.accept();

      // We don't check nreqs and relays, assuming they are correct by
      // construction. Anyway, if false, we are fucked.
      g_nreqs = nreqs ;
      uint256 len = relays.length;
      for ( uint8 i = 0 ; i < len ; i++ ){
        g_relays[ relays[i] ] = indexes[i] ;
      }
      g_expiration_date = expiration_date;
      g_event_address = event_address ;
      IDuneEvents( g_event_address )
        .userSwapDeployed( tvm.pubkey(), address(this) );
    }




  function _isRelay( uint256 key ) private view returns ( uint8 index ) {
    index = _isRelayExn(key, EXN_AUTH_FAILED);
  }




  function confirmOrder(
                         string order_id,
                         uint256 hashed_secret ,
                         uint64 ton_amount,
                         uint64 dun_amount,
                         address dest,
                         address depool
                         ) public
  {
    uint8 index = _isRelay( msg.pubkey () );
    require( uint64(now) < g_expiration_date, EXN_SWAP_EXPIRED );
    tvm.accept();

    SwapOrder order;

    optional(SwapOrder) opt = g_orders.fetch( order_id );
    if( opt.hasValue() ){
      order = opt.get();
    } else {
      order = SwapOrder
        (
         order_id,
         hashed_secret,
         ton_amount,
         dun_amount,
         dest,
         depool,
         STATE_WAITING_FOR_CONFIRMATION,
         0, // state_count
         0, // nreqs
         0, // voteMask
         uint64(now) + SWAP_EXPIRATION_TIME // expiration date
         );
    }

    // prevent confirmation after expiration date
    require( uint64(now) < order.expiration_date, EXN_SWAP_EXPIRED );

    uint64 bit = uint64(1) << index ;
    require(  ( order.voteMask & bit ) == 0 , EXN_ALREADY_CONFIRMED );
    order.voteMask |= bit ;
    order.nreqs++ ;
    g_orders [ order_id ] = order;

    IDuneEvents( g_event_address )
      .orderConfirmedByRelay( tvm.pubkey(), order_id, msg.pubkey() );

    if( order.nreqs >= g_nreqs &&
        order.state == STATE_WAITING_FOR_CONFIRMATION ) {
      order = _orderStateChanged( order, STATE_FULLY_CONFIRMED );

      _requestCredit( order );
    }
  }


  function requestCredit( string order_id ) public
    AuthOwnerOrRelay()
  {
    require( uint64(now) < g_expiration_date, EXN_SWAP_EXPIRED );
    SwapOrder order = g_orders [ order_id ];
    tvm.accept();

    _requestCredit( order );
  }



  function _requestCredit( SwapOrder order ) private
  {
    require( order.state == STATE_FULLY_CONFIRMED
             || order.state == STATE_CREDIT_DENIED, EXN_UNKNOWN_ORDER );

    IDuneGiver( s_giver_address ).
      creditOrder(order.order_id,
                  order.ton_amount,
                  tvm.pubkey());

    order = _orderStateChanged( order, STATE_WAITING_FOR_CREDIT ) ;
  }







  function revealOrderSecret( string order_id, string secret ) public
    AuthOwnerOrRelay()
  {
    require( uint64(now) < g_expiration_date, EXN_SWAP_EXPIRED );
    tvm.accept();

    optional(SwapOrder) opt = g_orders.fetch( order_id );
    require( opt.hasValue() , EXN_UNKNOWN_ORDER );
    SwapOrder order = opt.get();

    require( order.state == STATE_CREDITED, EXN_NOT_YET_CREDITED );

    // prevent secret revelation after expiration date
    require( uint64(now) < order.expiration_date, EXN_ORDER_EXPIRED );

    uint256 computed_hash = sha256( secret.toSlice() );
    require( computed_hash == order.hashed_secret, EXN_WRONG_SECRET );

    order = _orderStateChanged( order, STATE_REVEALED ) ;

    IDuneEvents( g_event_address )
      .orderSecretRevealed(tvm.pubkey(),
                           order_id,
                           secret);

    _maybeTransferOrder( order );
  }




  function transferOrder( string order_id ) public AuthOwnerOrRelay()
  {
    SwapOrder order = g_orders [ order_id ];
    require( order.state == STATE_REVEALED ||
             order.state == STATE_DEPOOL_DENIED, EXN_NOT_YET_REVEALED );
    tvm.accept();
    _maybeTransferOrder( order );
  }

  function cancelOrder( string order_id ) public AuthOwnerOrRelay()
  {
    SwapOrder order = g_orders [ order_id ];
    require( order.state != STATE_WAITING_FOR_CREDIT &&
             order.state != STATE_WAITING_FOR_DEPOOL &&
             order.state != STATE_REVEALED &&
             order.state != STATE_TRANSFERRED &&
             order.state != STATE_DEPOOL_DENIED &&
             order.state != STATE_CANCELLED
             , EXN_NOT_CANCELLABLE );
    require( uint64(now) > order.expiration_date, EXN_NOT_YET_EXPIRED );
    tvm.accept();

    if( order.state == STATE_CREDITED ){

      s_giver_address.transfer({
        value: order.ton_amount,
            bounce: false, flag: 0 });

    }
    order = _orderStateChanged( order, STATE_CANCELLED ) ;
  }



  function receiveCredit( string order_id ) public override AuthGiver()
  {
    SwapOrder order = g_orders[ order_id ];
    order = _orderStateChanged( order, STATE_CREDITED ) ;
  }






  function creditDenied( string order_id ) public override AuthGiver()
  {
    tvm.accept();
    SwapOrder order = g_orders[ order_id ];
    order = _orderStateChanged( order, STATE_CREDIT_DENIED ) ; 
  }





  function _maybeTransferOrder( SwapOrder order ) private {
    
    if ( order.state == STATE_REVEALED ||
         order.state == STATE_DEPOOL_DENIED ){

      if ( order.depool == address(0) ){

        order = _orderStateChanged( order, STATE_TRANSFERRED ) ;

        order.dest.transfer({
          value: order.ton_amount,
              bounce: false, flag: 1 });

      } else {

        order = _orderStateChanged( order, STATE_WAITING_FOR_DEPOOL ) ;

        require( ! g_depool_order.hasValue(),
                 EXN_FORMER_DEPOOL_ORDER_NOT_FINISHED );

        g_depool_order.set( order.order_id );
        IDePool( order.depool ).addVestingStake {
            flag: 1,
            value: order.ton_amount + DEPOOL_FEE }
        ( order.ton_amount,
          order.dest,
          uint32(MONTH_SECONDS),
          uint32(16) * uint32(MONTH_SECONDS) ); // 16 months
      }
    } 
  }





  function closeSwap() public AuthOwnerOrRelay()
  {
    require( uint64(now) > g_expiration_date, EXN_SWAP_NOT_EXPIRED );
    tvm.accept();
    selfdestruct( s_giver_address );
  }




  function receiveAnswer(uint32 errcode,
                         uint64 //comment
                         ) public override
  {
    string order_id = g_depool_order.get();
    SwapOrder order = g_orders [  order_id ];

    require( msg.sender == order.depool , EXN_AUTH_FAILED );
    require( order.state == STATE_WAITING_FOR_DEPOOL,
             EXN_UNEXPECTED_ANSWER_FROM_DEPOOL );

    g_depool_order.reset();

    if ( errcode == 0 ){ // SUCCESS
      order = _orderStateChanged( order, STATE_TRANSFERRED ) ; 
    } else {
      order = _orderStateChanged( order, STATE_DEPOOL_DENIED ) ; 
    }
  }




  function updateRelays( uint256[] relays, uint8[] indexes,
                         uint8 nreqs) public override AuthRoot()
  {
    mapping ( uint256 => uint8 ) map ;
    uint256 len = relays.length;
    for ( uint8 i = 0 ; i < len ; i++ ){
      map[ relays[i] ] = indexes[i] ;
    }
    g_relays = map;
    g_nreqs = nreqs;
  }

  function _orderStateChanged( SwapOrder order, uint8 state ) private
    returns ( SwapOrder modified_order )
  {
    order.state = state ;
    order.state_count ++;
    g_orders[ order.order_id ] = order ;
    emit OrderStateChanged( order.order_id, state) ;
    IDuneEvents( g_event_address )
      .orderStateChanged(
                         tvm.pubkey(), order.order_id,
                         order.state_count, state );
    modified_order = order ;
  }


  fallback () external {}
  receive () external {}  // same as default

  


  function get() public view returns
    (
     uint8 nreqs,
     uint256[] relays,
     uint8[] indexes,
     address root_address,
     address giver_address,
     address event_address,

     uint64 expiration_date,
     string depool_order,
     string[] order_ids,
     uint128[] order_amounts
     )
  {
    nreqs = g_nreqs;
    optional(uint256,uint8) opt_relay = g_relays.min() ;
    while( opt_relay.hasValue() ){
      (uint256 relay, uint8 index) = opt_relay.get();
      relays.push( relay );
      indexes.push( index );
      opt_relay = g_relays.next(relay);
    }
    root_address = s_root_address ;
    giver_address = s_giver_address ;
    event_address = g_event_address ;

    if( g_depool_order.hasValue() ){
      depool_order = g_depool_order.get();
    } else {
      depool_order = "";
    }
    expiration_date = g_expiration_date ;

    optional(uint256,SwapOrder) opt_swap = g_orders.min() ;
    while( opt_swap.hasValue() ){
      (uint256 order_id, SwapOrder swap) = opt_swap.get();
      order_ids.push( swap.order_id );
      order_amounts.push( swap.ton_amount );
      opt_swap = g_orders.next(order_id);
    }
  }


  function getOrder( string order_id )
    public view returns ( SwapOrder order, uint64 remaining_time )
  {
    order = g_orders[ order_id ];
    uint64 now64 = uint64(now);
    if( now64 > g_expiration_date )
      remaining_time = 0 ;
    else
      remaining_time = g_expiration_date - now64 ;
  }

}

