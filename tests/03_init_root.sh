#!/bin/bash

. ./env.sh

FT_SWITCH=${NETWORK}
export FT_SWITCH




$FT node --give giver_address:40000

$FT call giver_address init '{ "root_address": "%{account:address:root_address}", "event_address": "%{account:address:event_address}" }' || exit 2

# These 3 calls should return the same address
$FT call giver_address getUserSwapAddress '{ "pubkey": "0x%{account:pubkey:relay1}" }' --run --subst '@%{res:addr}' -o giver_address.calc || exit 2
$FT call root_address getUserSwapAddress '{ "pubkey": "0x%{account:pubkey:relay1}" }' --run --subst '@%{res:addr}' -o root_address.calc || exit 2

if [ "$(cat giver_address.calc)" != "$(cat root_address.calc)" ]; then
    echo "root contract was not properly initialized"
    exit 2
fi

$FT call event_address getUserSwapAddress '{ "pubkey": "0x%{account:pubkey:relay1}" }' --run --subst '@%{res:addr}' -o event_address.calc || exit 2

if [ "$(cat giver_address.calc)" != "$(cat event_address.calc)" ]; then
    echo "event contract was not properly initialized"
    exit 2
fi



# $FT call root_address deployUserSwap '{ "pubkey": "0x%{account:pubkey:relay1}" }'  --sign relay1 || exit 2

# Should be the same address as returned above

