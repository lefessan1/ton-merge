
./00_init.sh
./01_account.sh
./02_deploy.sh
./03_cleardb.sh

ft watch --account root_address

ft watch --account event_address

../bin/freeton-relay --db-to-ft

Should generate contract:

0:38990b33bdaeebd67b3b13543cca2f1f8d31647ca71d27e2794be4f749c31af4

ft call event_address getUserSwapAddress '{ "pubkey": "0xfd9ee2babfa35b65917f732316dbb3d31935ccacd2a0aa92e043f8c762e0da28" }' --run


ft account --create user0_contract --address 0:7b069672d66d6b2195d5222627d875f93ec2d9262fde0579a44ecb5bb7a8c3e4 --contract DuneUserSwap


ft watch --account user0_contract



TODO:
* Transactions received by DuneEvents have the aborted flag. Why ?

