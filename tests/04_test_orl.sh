#!/bin/bash

. ./env.sh

FT_SWITCH=${NETWORK}
export FT_SWITCH

# Compute the address of ORL's swap contract, use it to tell the Depool that
# we are going to vest some tokens

$FT call event_address getUserSwapAddress '{ "pubkey": "0x%{account:pubkey:orl}" }' --run --subst '@%{res:addr}' -o orl_swap_addr.calc || exit 2
$FT account --create orl_swap --address $(cat orl_swap_addr.calc) --contract DuneUserSwap

$FT multisig -a orl --transfer 0.1 --to depool setVestingDonor '{"donor" : "%{account:address:orl_swap}" }' || exit 2

$FT output --file test-swap-orl.json.in -o test-swap-orl.json || exit 2


$FT inspect -h --shard-account orl_swap --subst @%{res:0:id} --output orl_swap.block

$FT watch --account orl_swap &> watch-orl.log &
