#!/bin/bash

. ./env.sh

FT_SWITCH=${NETWORK}
export FT_SWITCH


# should be between 0 and 9
export TON_SWAP=$1

$FT call event_address getUserSwapAddress '{ "pubkey": "0x%{account:pubkey:user%{env:TON_SWAP}}" }' --run --subst '@%{res:addr}' -o orl_swap_10${TON_SWAP}.calc || exit 2
$FT account --create user10${TON_SWAP}_swap --address $(cat orl_swap_10${TON_SWAP}.calc) --contract DuneUserSwap


export RELAY=1
export TON_MERGE_DB="${RELAY1_DATABASE}"
export TON_MERGE_PASSPHRASE="${RELAY1_PASSPHRASE}"

cmd ../bin/freeton-relay --test-swap test-swap-10${TON_SWAP}.json

export RELAY=2
export TON_MERGE_DB="${RELAY2_DATABASE}"
export TON_MERGE_PASSPHRASE="${RELAY2_PASSPHRASE}"

cmd ../bin/freeton-relay --test-swap test-swap-10${TON_SWAP}.json

export RELAY=3
export TON_MERGE_DB="${RELAY3_DATABASE}"
export TON_MERGE_PASSPHRASE="${RELAY3_PASSPHRASE}"

cmd ../bin/freeton-relay --test-swap test-swap-10${TON_SWAP}.json
