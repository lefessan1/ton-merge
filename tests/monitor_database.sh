#!/bin/bash

. ./env.sh

FT_SWITCH=${NETWORK}
export FT_SWITCH

case $1 in
    "")
     TON_MERGE_DB="${RELAY1_DATABASE}"
     TON_MERGE_PASSPHRASE="${RELAY1_PASSPHRASE}"
     ;;
    1)
     TON_MERGE_DB="${RELAY1_DATABASE}"
     TON_MERGE_PASSPHRASE="${RELAY1_PASSPHRASE}"
     ;;
    2)
     TON_MERGE_DB="${RELAY2_DATABASE}"
     TON_MERGE_PASSPHRASE="${RELAY2_PASSPHRASE}"
     ;;
    3)
     TON_MERGE_DB="${RELAY3_DATABASE}"
     TON_MERGE_PASSPHRASE="${RELAY3_PASSPHRASE}"
     ;;
    *) echo "Don't know what to do with $1"
       exit 2
esac
     
     
echo TON_MERGE_DB=$TON_MERGE_DB

../bin/freeton-relay --db-to-ft
