#!/bin/bash

. ./env.sh

FT_SWITCH=${NETWORK}
export FT_SWITCH


export RELAY=1
export TON_MERGE_DB="${RELAY1_DATABASE}"
export TON_MERGE_PASSPHRASE="${RELAY1_PASSPHRASE}"

echo COMMAND ../bin/freeton-relay --db-to-ft '&>' relay${RELAY}_database.log
cmd ../bin/freeton-relay --db-to-ft &> relay${RELAY}_database.log &
echo COMMAND ../bin/freeton-relay --ft-to-db '&>' relay${RELAY}_freeton.log &
cmd ../bin/freeton-relay --ft-to-db &> relay${RELAY}_freeton.log &
cmd ../bin/freeton-relay --test-swap test-swap-orl.json

export RELAY=2
export TON_MERGE_DB="${RELAY2_DATABASE}"
export TON_MERGE_PASSPHRASE="${RELAY2_PASSPHRASE}"

echo COMMAND ../bin/freeton-relay --db-to-ft '&>' relay${RELAY}_database.log &
cmd ../bin/freeton-relay --db-to-ft &> relay${RELAY}_database.log &
echo COMMAND ../bin/freeton-relay --ft-to-db '&>' relay${RELAY}_freeton.log &
cmd ../bin/freeton-relay --ft-to-db &> relay${RELAY}_freeton.log &
cmd ../bin/freeton-relay --test-swap test-swap-orl.json


export RELAY=3
export TON_MERGE_DB="${RELAY3_DATABASE}"
export TON_MERGE_PASSPHRASE="${RELAY3_PASSPHRASE}"

echo COMMAND ../bin/freeton-relay --db-to-ft '&>' relay${RELAY}_database.log &
cmd ../bin/freeton-relay --db-to-ft &> relay${RELAY}_database.log &
echo COMMAND ../bin/freeton-relay --ft-to-db '&>' relay${RELAY}_freeton.log &
cmd ../bin/freeton-relay --ft-to-db &> relay${RELAY}_freeton.log &
cmd ../bin/freeton-relay --test-swap test-swap-orl.json
