#!/bin/bash

. ./env.sh

FT_SWITCH=${NETWORK}

export FT_SWITCH

$FT account --create admin --surf --passphrase "%{env:ADMIN_PASSPHRASE}" || exit 2
$FT node --give admin || exit 2
$FT config --deployer admin || exit 2

$FT account --create freeton_giver --surf --passphrase "%{env:FTGIVER_PASSPHRASE}" || exit 2
$FT node --give freeton_giver:95000 || exit 2

$FT account --create relay1 --surf --passphrase "%{env:RELAY1_PASSPHRASE}" || exit 2
$FT node --give relay1 || exit 2

$FT account --create relay2 --surf --passphrase "%{env:RELAY2_PASSPHRASE}" || exit 2
$FT node --give relay2 || exit 2

$FT account --create relay3 --surf --passphrase "%{env:RELAY3_PASSPHRASE}" || exit 2
$FT node --give relay3 || exit 2

$FT account --create orl --surf --passphrase "%{env:ORL_PASSPHRASE}" || exit 2
$FT node --give orl || exit 2

