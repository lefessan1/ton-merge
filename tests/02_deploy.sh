#!/bin/bash

. ./env.sh

FT_SWITCH=${NETWORK}
export FT_SWITCH




$FT account giver_address --delete

$FT contract --create giver_address --sign admin --deploy DuneGiver --params '{ "freeton_giver": "%{account:address:freeton_giver}" }' || exit 2

# $FT node --give giver_address:10 || exit 2

$FT account giver_address -v || exit 2

$FT watch --account giver_address &> watch-giver.log &





$FT account root_address --delete

$FT contract --create root_address --sign admin --deploy DuneRootSwap --params '{ "relays": [ "0x%{account:pubkey:relay1}",  "0x%{account:pubkey:relay2}",  "0x%{account:pubkey:relay3}" ], "nreqs": %{env:TON_NREQS}, "duneUserSwapCode": "%{get-code:contract:tvc:DuneUserSwap}", "giver_address": "%{account:address:giver_address}", "expiration_date": %{now:plus:10:day} }' || exit 2

# $FT node --give root_address:1000 || exit 2

$FT watch --account root_address &> watch-root.log &






$FT account event_address --delete

$FT contract --create event_address --sign admin --deploy DuneEvents --params '{ "giver_address": "%{account:address:giver_address}" }' || exit 2

$FT watch --account event_address &> watch-event.log &





$FT account --delete depool

$FT contract --create depool --deploy FakeDePool || exit 2

$FT watch --account depool &> watch-depool.log &







# These 3 calls should return the different addresses, because init not called
$FT call giver_address getUserSwapAddress '{ "pubkey": "0x%{account:pubkey:relay1}" }' --run || exit 2
$FT call root_address getUserSwapAddress '{ "pubkey": "0x%{account:pubkey:relay1}" }' --run || exit 2
$FT call event_address getUserSwapAddress '{ "pubkey": "0x%{account:pubkey:relay1}" }' --run || exit 2

$FT inspect -h --shard-account giver_address --subst @%{res:0:id} --output giver_address.block

$FT inspect -h --shard-account root_address --subst @%{res:0:id} --output root_address.block

$FT inspect -h --shard-account event_address --subst @%{res:0:id} --output event_address.block



