#!/bin/bash

. ./env.sh

$FT switch mainnet || exit 2

FT_SWITCH=${NETWORK}

rm -f *.block *.calc

killall ft
killall freeton-relay

$FT --switch ${NETWORK} node --stop || exit 2
$FT switch --remove ${NETWORK} || exit 2
$FT switch --create ${NETWORK} || exit 2

export FT_SWITCH

$FT node --start || exit 2
echo
echo Waiting 10 seconds for warmup
sleep 10

