#!/bin/bash

. ./env.sh

FT_SWITCH=${NETWORK}
export FT_SWITCH




export TON_MERGE_DB="${RELAY1_DATABASE}"
export TON_MERGE_PASSPHRASE="${RELAY1_PASSPHRASE}"
  
cmdk dropdb ${TON_MERGE_DB}
cmdk createdb ${TON_MERGE_DB}
cmd ../bin/freeton-relay --init-db
$FT client --exec --  ../bin/freeton-relay --giver "%{account:address:giver_address}" || exit 2





export TON_MERGE_DB="${RELAY2_DATABASE}"
export TON_MERGE_PASSPHRASE="${RELAY2_PASSPHRASE}"
  
cmdk dropdb ${TON_MERGE_DB}
cmdk createdb ${TON_MERGE_DB}
cmd ../bin/freeton-relay --init-db
$FT client --exec --  ../bin/freeton-relay --giver "%{account:address:giver_address}" || exit 2






export TON_MERGE_DB="${RELAY3_DATABASE}"
export TON_MERGE_PASSPHRASE="${RELAY3_PASSPHRASE}"
  
cmdk dropdb ${TON_MERGE_DB}
cmdk createdb ${TON_MERGE_DB}
cmd ../bin/freeton-relay --init-db
$FT client --exec --  ../bin/freeton-relay --giver "%{account:address:giver_address}" || exit 2

