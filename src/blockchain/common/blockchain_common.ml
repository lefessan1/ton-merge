open Lwt_utils
open Data_types

module Swap_contract =
  Spice_swapper.Contracts.Swap(struct
    let node_url () =
      Config.get_nodes () >|= List.hd
  end)

module Swap_code =
  Spice_swapper.Code.Swap


let timestamp_to_zstr t =
  CalendarLib.Calendar.to_unixfloat t |> Z.of_float |> Z.to_string

let swap_storage_to_swapper_info s =
  {
    swap_start = Encoding_common.cal_of_str s.Swap_contract.swap_start;
    swap_end = Encoding_common.cal_of_str s.swap_end;
    swap_span = s.swap_span;
    total_vouched = s.total_vouched;
    vested_vouched = s.vested_vouched;
    swapped = s.swapped;
    vested_threshold = s.vested_threshold;
    total_exchangeable = s.total_exchangeable;
    locked = s.locked;
  }

let dune_expr enc =
  Json_encoding.obj1 (Json_encoding.req "dune_expr" enc)

let tr_params_of_string params enc =
  Ezjsonm.value_from_string params |>
  Json_encoding.destruct (dune_expr enc)
