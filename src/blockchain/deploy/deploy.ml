open Lwt_utils
open Data_types
open Encoding_common
open Blockchain_common

type t = {
  deployer_alias : string;
  delegate: string option;
  admin: string;
  swap_start: timestamp;
  swap_end: timestamp;
  swap_span: int;
  vested_threshold: Z.t;
  total_exchangeable: Z.t;
}

let contract_encoding =
  let open Json_encoding in
  conv
    (fun {deployer_alias; delegate; admin; swap_start; swap_end; swap_span;
          vested_threshold; total_exchangeable} ->
      (deployer_alias, delegate, admin, swap_start, swap_end, swap_span,
       vested_threshold, total_exchangeable))
    (fun (deployer_alias, delegate, admin, swap_start, swap_end, swap_span,
          vested_threshold, total_exchangeable) ->
      {deployer_alias; delegate; admin; swap_start; swap_end; swap_span;
       vested_threshold; total_exchangeable}) @@
  obj8
    (req "deployer_alias" string)
    (opt "delegate" string)
    (req "admin" string)
    (req "swap_start" timestamp)
    (req "swap_end" timestamp)
    (req "swap_span" int)
    (req "vested_threshold" amount)
    (req "total_exchangeable" amount)

let encoding =
  let open Json_encoding in
  conv_ignore_extra
    (fun x -> x) (fun x -> x)
    (obj1 (req "contract" contract_encoding))

let read_config_from_file f =
  let in_ch = open_in f in
  let json = Ezjsonm.from_channel in_ch in
  close_in in_ch;
  try
    Json_encoding.destruct encoding json
  with exn ->
    Format.eprintf "Could not read config file %s: @[<hv>%a@]@."
      f (Json_encoding.print_error ?print_unknown:None) exn;
    raise exn

let aps (EzAPI.TYPES.BASE node) =
  let uri = Uri.of_string node in
  let tls = match Uri.scheme uri with
    | Some "https" -> true
    | Some "http" | None -> false
    | Some s -> failwith ("unuspported node scheme "^ s) in
  let port = match Uri.port uri with
    | Some p -> p
    | None when tls -> 443
    | None -> 80 in
  let host = match Uri.host uri with
    | None -> "localhost"
    | Some s -> s in
  host, port, tls

let () = Random.self_init ()

let deploy ~force file =
  let> () =
    Db.get_swap_contract_address () >|= function
    | Some s when not force ->
      Format.ksprintf failwith
        "Swap contract already deployed at %s (use --force to redeploy)" s
    | _ -> () in
  let contract = read_config_from_file file in
  let> node = Config.get_nodes () >|= List.hd in
  let host, port, tls = aps node in
  let param_json =
    Json_encoding.construct
      Swap_contract.__init_storage_parameter_enc
      (contract.admin,
       contract.swap_start |> timestamp_to_zstr,
       contract.swap_end |> timestamp_to_zstr,
       contract.swap_span,
       contract.vested_threshold,
       contract.total_exchangeable) in
  let param_json_s = Ezjsonm.value_to_string param_json in
  let name_contract = Printf.sprintf "swap_%ld" (Random.int32 Int32.max_int) in
  let code_json = Ezjsonm.value_from_string Swap_code.mycode in
  let code_json = match code_json with
    | `O [ "dune_code", code ] -> code
    | _ -> code_json in
  let code_file, oc = Filename.open_temp_file name_contract ".json" in
  output_string oc @@
    "#love-json\n" ^
    Ezjsonm.value_to_string code_json;
  close_out oc;
  let cmd = Format.sprintf
    "dune-client --wait none -A %s -P %d %s \
     originate contract %s \
     transferring 0 from %s \
     running %s \
     --init '#love-json:%s' \
     %s \
     --burn-cap 20 --force --no-locs -q"
    host
    port
    (if tls then "-S" else "")
    name_contract
    contract.deployer_alias
    code_file
    param_json_s
    (match contract.delegate with
     | None -> ""
     | Some d -> "--delegate " ^ d)
  in
  Format.eprintf "Deploy command: \n%s\n@." cmd;
  let res = Sys.command cmd in
  if res <> 0 then
    Format.ksprintf failwith "Error %d on deploy" res;
  let kt1_file = Filename.temp_file name_contract ".kt1" in
  let cmd =
    Format.sprintf
      "dune-client show known contract %s 2> /dev/null | tr -d '[:space:]' > %s"
      name_contract kt1_file in
  let res = Sys.command cmd in
  if res <> 0 then
    Format.ksprintf failwith "Error %d: unknown contract %s" res name_contract;
  let ic = open_in kt1_file in
  let kt1 = input_line ic in
  close_in ic;
  Format.printf "Swap contract is %s, saving to database ...@." kt1;
  let> () = Db.register_swap_contract_address ~addr:kt1 in
  (* let> block_time = Config.get_block_time () in *)
  let rec get_storage retry =
    if retry = 0 then failwith "Could not read storage for contract";
    Format.printf "Sleeping for %d s before retrieving storage...@." 5;
    let> () = Lwt_unix.sleep 5. in
    Swap_contract.get_storage ?block:None kt1 >>= function
    | Error _ -> get_storage (retry - 1)
    | Ok x -> Lwt.return x
  in
  let> storage = get_storage 30 in
  let EzAPI.TYPES.BASE n = node in
  let> level =
    EzCurl_lwt.get (EzAPI.TYPES.URL (n ^ "chains/main/blocks/head/header")) >|= function
    | Error (code, _) ->
      Format.ksprintf failwith "Cannot get head level, http code %d" code
    | Ok j ->
      let j = Ezjsonm.value_from_string j in
      Ezjsonm.find j ["level"] |> Ezjsonm.get_int
  in
  Format.printf "First level is %d, saving to database ...@." level;
  let> () = Config.set_first_level level in
  Format.printf "Saving contract parameters to database ...@.";
  let swapper_info = swap_storage_to_swapper_info storage in
  let> () = Db.set_swapper_info swapper_info in
  let swaps_big_map = match storage.swaps with
    | None -> assert false
    | Some bmid -> Z.to_int bmid in
  let>! () = Db.set_swaps_big_map swaps_big_map in
  Format.printf "\nDONE.@."
