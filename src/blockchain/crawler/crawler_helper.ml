open Lwt_utils
open Data_types
open Dune.Types
open Blockchain_common

let address_known_in_system addr =
  Db.address_known_in_db addr

let op_metadata = function
  | NTransaction tr ->
    tr.node_tr_metadata
    |> Option.map (fun x -> x.manager_meta_operation_result)
    |> Option.value ~default:None
  | NOrigination ori ->
    ori.node_or_metadata
    |> Option.map (fun x -> x.manager_meta_operation_result)
    |> Option.value ~default:None
  | _ -> None

let internal_operations = function
  | NTransaction tr ->
    tr.node_tr_metadata
    |> Option.map (fun x -> x.manager_meta_internal_operation_results)
    |> Option.value ~default:[]
  | NOrigination ori ->
    ori.node_or_metadata
    |> Option.map (fun x -> x.manager_meta_internal_operation_results)
    |> Option.value ~default:[]
  | _ -> []

let is_success op =
  match op_metadata op with
  | Some { meta_op_status = Some "applied"; _ } -> true
  | _ -> false

let metadata_addresses = function
  | Some { manager_meta_operation_result = Some op_res ; _ } ->
    op_res.meta_op_originated_contracts
  | _ -> []

let origination_addresses (ori : node_origination) =
  ori.node_or_src ::
  metadata_addresses ori.node_or_metadata

let transaction_addresses (tr : node_transaction) =
    tr.node_tr_src ::
    tr.node_tr_dst ::
    metadata_addresses tr.node_tr_metadata

let keep_operation op =
  let addresses = match op with
  | NTransaction tr -> transaction_addresses tr
  | NOrigination ori -> origination_addresses ori
  | _ -> [] in
  Lwt_list.exists_p address_known_in_system addresses


(* THIS CODE IS DUPLICATED in freeton.ml *)

(* ton_amount (nano) = ( dun_amount (mu) * 10 * 1000 ) / 503 *)
let dun_ton_mul = Z.of_string "10_000"
let dun_ton_div = Z.of_string "503"
let ton_of_dun dun_amount =
  Z.div ( Z.mul dun_amount dun_ton_mul ) dun_ton_div

let handle_big_map_update ~confirmed ~op_hash:_ ~op_index:_ ~internal_index:_ ~swaps_bmid i =
  match i.bmd_big_map with
  | Some id when Z.equal id (Z.of_int swaps_bmid) -> (
      match i.bmd_action with
      | Some "update" -> (
          match i.bmd_key with
          | Some DuneExpr LoveExpr LoveValue Love_value.Value.VNat id -> (
              let swap_id = Z.to_int id in
              match i.bmd_value with
              | None -> (* remove *)
                Format.eprintf "Cancel swap %d@." swap_id;
                let> () = Db.SWAPS.set_dune_status ~swap_id
                    (if confirmed then SwapRefunded else SwapRefundAsked) in
                Lwt.return_nil
              | Some DuneExpr LoveExpr LoveValue love_swap_e ->
                let swap_json =
                  Love_json_encoding.ValueEncoder.encode_value love_swap_e in
                let love_swap =
                  Json_encoding.destruct
                    Swap_contract.swap_t_encoding swap_json in
                let dun_amount = love_swap.amount in
                let ton_amount = ton_of_dun dun_amount in
                let> old_status = Db.SWAPS.get_dune_status ~swap_id in
                (
                  match old_status with
                  | None ->
                    let dune_status = match love_swap.status with
                      | Swap_contract.Pending () ->
                        if confirmed then SwapConfirmed else SwapSubmitted
                      | Swap_contract.Swapped () -> assert false
                    in
                    let swap = {
                      swap_id;
                      dune_origin = love_swap.origin;
                      dun_amount = dun_amount;
                      ton_amount = ton_amount ;
                      dune_status;
                      freeton_status = SwapWaitingForConfirmation;
                      hashed_secret = love_swap.hashed_secret;
                      time = Encoding_common.cal_of_str love_swap.time;
                      refund_address = love_swap.return_address;
                      freeton_address = love_swap.freeton_address;
                      freeton_depool = love_swap.freeton_depool ;
                      freeton_pubkey = love_swap.freeton_pubkey ;
                      confirmations = 0;
                      secret = None ;
                      logical_time = 0L ;
                    } in
                    let> () = Db.SWAPS.add swap in
                    Lwt.return [swap]
                  | Some _old_status ->
                    let status = match love_swap.status with
                      | Swap_contract.Pending () ->
                        if confirmed then SwapConfirmed else SwapSubmitted
                      | Swap_contract.Swapped () -> SwapCompleted
                    in
                    let> () = Db.SWAPS.set_dune_status ~swap_id status in
                    Lwt.return_nil
                )
              | Some _ -> Lwt.return_nil
            )
          | _ -> Lwt.return_nil
        )
      | _ -> Lwt.return_nil
    )
  | _ -> Lwt.return_nil

let print_swap swap =
  Format.printf "\n    #%d: %s DUN  / %s ==> %s%s [Hash secret: %s]"
    swap.swap_id
    (Encoding_common.string_of_amount swap.dun_amount)
    swap.dune_origin
    swap.freeton_address
    (match swap.freeton_depool with None -> "" | Some d -> " [VESTED to depool %s]" ^ d)
    (Hex.show (Hex.of_bytes swap.hashed_secret))

(*  This is done in the Free TON relay

let handle_new_swap ~confirmed swap =
  if confirmed && swap.status = SwapPending then
    let> already_allocated = Db.get_swap_allocated swap.swap_id in
    if already_allocated then begin
      Format.printf " | ALREADY ALLOCATED";
      Lwt.return_unit
    end
    else
      let> () = FreeTON.create_allocation swap in
      Db.mark_swap_allocated swap.swap_id
  else Lwt.return_unit
*)

let handle_swaps ~confirmed new_swaps =
  Format.printf "    %sonfirmed swaps [%d] :"
    (if confirmed then "C" else "Unc") (List.length new_swaps);
  Lwt_list.iter_s (fun swap ->
      print_swap swap; Lwt.return_unit
      (* handle_new_swap ~confirmed swap *)
    ) new_swaps >|= fun () ->
  Format.print_newline ()

let handle_big_map_diff dbh ~confirmed ~op_hash ~op_index ~internal_index ~op dune_op =
  match op_metadata dune_op with
  | None -> Lwt.return_unit
  | Some metadata ->
    match metadata.meta_op_big_map_diff with
    | None | Some [] -> Lwt.return_unit
    | Some bm_diff ->
      let> swaps_bmid = Db.get_swaps_big_map ()
        >|= function
        | None -> failwith "Swaps big maps not set"
        | Some id -> id in
      let> new_swaps =
        Lwt_list.fold_left_s (fun infos i ->
            let>! bupds =
              handle_big_map_update ~confirmed
                ~op_hash ~op_index ~internal_index ~swaps_bmid i in
            List.rev_append bupds infos
          ) [] bm_diff in
      let new_swaps = List.rev new_swaps in
      (* Register swap id in operation when necessary *)
      let> () = match op, new_swaps with
       | Some (op_id, Deposit _), [swap] ->
         Db.set_swap_id_for_deposit dbh ~op_id ~swap_id:swap.swap_id
       | _ -> Lwt.return_unit
      in
      if new_swaps <> [] then
        handle_swaps ~confirmed new_swaps
      else
        Lwt.return_unit

let handle_single_operation dbh
    ~confirmed ~block_hash ~op_hash ~internal_index ~op_index dune_op =
  let> swap_address = Db.get_swap_contract_address () >|= function
    | None -> failwith "Swap contract not registered"
    | Some a -> a in
  let status = match confirmed with
    | false -> Unconfirmed {
        block = block_hash;
        operation_hash = op_hash;
        index = op_index;
        internal_index = internal_index;
        confirmations = 0;
      }
    | true -> Confirmed {
        block = block_hash;
        operation_hash = op_hash;
        index = op_index;
        internal_index = internal_index;
      } in
  match dune_op with
  | NTransaction tr when tr.node_tr_dst = swap_address -> (
      let source = tr.node_tr_src in
      match tr.node_tr_entrypoint, tr.node_tr_parameters with

      (* Deposit *)
      | Some "deposit", Some params ->
        let (hashed_secret, return_address, freeton_address, freeton_pubkey, freeton_depool) =
          tr_params_of_string params
            Swap_contract.deposit_parameter_enc in
        let op = Deposit {
            hashed_secret ;
            origin_address = tr.node_tr_src;
            amount = Z.of_int64 tr.node_tr_amount;
            return_address;
            freeton_address;
            freeton_pubkey;
            freeton_depool;
            swap_id = None;
          } in
        let>! id = Db.add_operation dbh ~source ~status op in
        Some (id, op)

      (* Swap *)
      | Some "swap", Some params ->
        let (swap_id, secret) =
          tr_params_of_string params
            Swap_contract.swap_parameter_enc in
        let swap_id = Z.to_int swap_id in
        let op = Swap {
            swap_id;
            secret;
          } in
        let> id = Db.add_operation dbh ~source ~status op in
        let>! () = Db.SWAPS.set_secret ~swap_id secret in
        Some (id, op)

      (* Refund *)
      | Some "refund", Some params ->
        let (swap_id) =
          tr_params_of_string params
            Swap_contract.refund_parameter_enc in
        let op = Refund {
            swap_id = Z.to_int swap_id ;
          } in
        let>! id = Db.add_operation dbh ~source ~status op in
        Some (id, op)

      | _ ->
        (* ignore other transactions *)
        Lwt.return_none
    )
  | _ ->
    (* ignore other operations *)
    Lwt.return_none


let update_swapper_info () =
  let> kt1 = Db.get_swap_contract_address () >|= function
    | None -> failwith "Swapper contract unregistered"
    | Some k -> k in
  let> storage = Swap_contract.get_storage ?block:None kt1 >|= function
    | Error _ -> failwith "Could not retrieve storage"
    | Ok x -> x in
  let swapper_info = swap_storage_to_swapper_info storage in
  let> () = Db.set_swapper_info swapper_info in
  let swaps_big_map = match storage.swaps with
    | None -> assert false
    | Some bmid -> Z.to_int bmid in
  let>! () = Db.set_swaps_big_map swaps_big_map in
  ()


let handle_generic_operation dbh ~confirmed ~block_hash ~op_hash ~op_index dune_op =
  let internal_index = ref (-1) in
  let rec handle_generic_operation ~confirmed ~block_hash ~op_hash ~op_index dune_op =
    incr internal_index;
    let> op = handle_single_operation dbh ~confirmed ~block_hash ~op_hash
        ~internal_index:!internal_index~op_index dune_op in
    let> () =
      handle_big_map_diff dbh ~confirmed ~op_hash
        ~internal_index:!internal_index ~op_index ~op dune_op in
    (* let> () =
     *   if confirmed then
     *     Lwt.return_unit
     *   else Lwt.return_unit in *)
    Lwt_list.iter_s
      (handle_generic_operation ~confirmed ~block_hash ~op_hash ~op_index)
      (internal_operations dune_op)
  in
  let> () =
    handle_generic_operation ~confirmed ~block_hash ~op_hash ~op_index dune_op
  in
  let>! () =  update_swapper_info () in
  ()


module Unconfirmed = struct

  let register_new_operation _dbh op _dune_op =
    (match op.op_content with
    | Deposit _->
      Format.printf "New unconfirmed deposit %ld@." op.op_id;
    | Swap { swap_id; _ } ->
      Format.printf "New unconfirmed swap for deposit %d@." swap_id;
    | Refund { swap_id; _ }->
      Format.printf "New unconfirmed refund for deposit %d@." swap_id;
    );
    Lwt.return_ok ()
end

module Confirmed = struct
  let register_new_operation _dbh op _dune_op =
    (match op.op_content with
    | Deposit _->
      Format.printf "New confirmed deposit %ld@." op.op_id;
    | Swap { swap_id; _ } ->
      Format.printf "New confirmed swap for deposit %d@." swap_id;
    | Refund { swap_id; _ }->
      Format.printf "New confirmed refund for deposit %d@." swap_id;
    );
    Lwt.return_ok ()
end
