
open Lwt_utils
(* open Misc_db *)
open Data_types
open Dune.Types
open Db_common


let of_node_block b = {
  block_hash = b.node_hash;
  block_predecessor = Some b.node_header.header_shell.shell_predecessor;
  block_level = Int32.of_int b.node_header.header_shell.shell_level;
  block_timestamp =
    b.node_header.header_shell.shell_timestamp
    |> Date.to_string
    |> Encoding_common.cal_of_str
}

let register_block ~operations b =
  let b = of_node_block b in
  Format.printf "register      %s at level %ld [%s]%!"
    (String.sub b.block_hash 0 10) b.block_level (Encoding_common.cal_to_str b.block_timestamp);
  transaction @@ fun dbh ->
  Db.register_block_if_unknown dbh b >>= function
  | `AlreadyKnown main ->
    Format.printf " : already known (main = %b)@." main;
    Lwt.return_ok main
  | `New ->
    Format.printf " : ok (with %d operations)@." (List.length operations);
    let> () = Crawler_operations.register_operations dbh b operations in
    Lwt.return_ok false

let set_main ?(main=true) dbh ~block_hash =
  let> marked_levels = Db.mark_block_main dbh ~main ~block_hash in
  match marked_levels with
  | [] -> Lwt.return_ok ()
  | _ :: _ :: _ -> Lwt.return_error "Several blocks with same hash!"
  | [ level ] ->
    let> updated_operations =
      if main then
        let> () = Db.mark_operations_in_main dbh ~block_hash in
        let> confirmations_needed = Config.get_confirmations () in
        Db.update_confirmations_operations ~confirmations_needed dbh level
      else
        let>! () = Db.mark_operations_in_alt dbh ~block_hash in
        []
    in
    let@ () =
      Crawler_operations.update_operations_info dbh ~main updated_operations in
    let>+ () =
      Crawler_operations.update_block_operations ~main dbh ~block_hash ~level in
    ()

let set_main_dbh ?main block_hash =
  transaction @@ fun dbh ->
  set_main ?main dbh ~block_hash
