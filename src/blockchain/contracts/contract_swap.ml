open Json_utils
open Request_utils
module Interface (R : REQUEST) (I : INFO with type url = R.url) = struct
  let has_storage = false
  let storage_encoding = unit_encoding



  (**  Preliminary code  **)
  open Swap_code




  (* Type declaration *)
  type nonrec swap_status =
      Pending of unit
    | Swapped of unit
  let swap_status_encode =(fun c -> match c with
        Pending (id) ->
        let local_encoding = unit_encoding in
        `O ["constr",`A [`String "Pending";`A [Json_encoding.construct (local_encoding) id]]]
      |  Swapped (id) ->
        let local_encoding = unit_encoding in
        `O ["constr",`A [`String "Swapped";`A [Json_encoding.construct (local_encoding) id]]])
  let swap_status_decode =(fun c ->
      match c with
        `O ["constr",
            `A [`String "Pending";`A [id]]] -> let local_encoding = unit_encoding in
        Pending (Json_encoding.destruct local_encoding id)

      | `O ["constr",
            `A [`String "Swapped";`A [id]]] -> let local_encoding = unit_encoding in
        Swapped (Json_encoding.destruct local_encoding id)

      | _ ->  raise (DecodingError "Failing when decoding role"))
  let swap_status_encoding =Json_encoding.conv swap_status_encode swap_status_decode json
  let swap_status_example () =(chooseFrom
                                 [(fun () -> Pending (unit_example ()))
                                 ;
                                  (fun () -> Swapped (unit_example ()))] ())

  (* Type declaration *)
  type nonrec swap_t =  {
    origin : address;
    amount : dun;
    status : swap_status;
    hashed_secret : bytes;
    time : timestamp;
    return_address : keyhash;
    freeton_address : string;
    freeton_pubkey : string;
    freeton_depool : string option
  }
  let swap_t_encode (*: swap_t Json_encoding.encoding*) =
    let open Json_encoding in
    fun {origin;amount;status;hashed_secret;time;return_address;freeton_address;freeton_pubkey;freeton_depool} -> (origin,amount,status,hashed_secret,time,return_address,freeton_address,freeton_pubkey,freeton_depool)
  let swap_t_decode (*: swap_t Json_encoding.encoding*) =
    let open Json_encoding in
    fun (origin,amount,status,hashed_secret,time,return_address,freeton_address,freeton_pubkey,freeton_depool) -> {origin;amount;status;hashed_secret;time;return_address;freeton_address;freeton_pubkey;freeton_depool}

  let swap_t_encoding  (*:  swap_t Json_encoding.encoding*) =
    let open Json_encoding in
    conv swap_t_encode swap_t_decode
    @@ obj1(req "record" @@
            obj9
              (req "origin" address_encoding)
              (req "amount" dun_encoding)
              (req "status" swap_status_encoding)
              (req "hashed_secret" bytes_encoding)
              (req "time" timestamp_encoding)
              (req "return_address" keyhash_encoding)
              (req "freeton_address" string_encoding)
              (req "freeton_pubkey" string_encoding)
              (req "freeton_depool" (loveoption_encoding string_encoding)))
  let swap_t_example  () =
    {origin = (address_example ())
    ;amount = (dun_example ())
    ;status = (swap_status_example ())
    ;hashed_secret = (bytes_example ())
    ;time = (timestamp_example ())
    ;return_address = (keyhash_example ())
    ;freeton_address = (string_example ())
    ;freeton_pubkey = (string_example ())
    ;freeton_depool = ((loveoption_example string_example) ())
    }

  (* Type declaration *)
  type nonrec storage =  {
    admin : address;
    swap_counter : nat;
    swaps : (nat, swap_t) bigmap;
    swap_start : timestamp;
    swap_end : timestamp;
    swap_span : int;
    total_vouched : dun;
    vested_vouched : dun;
    swapped : dun;
    vested_threshold : dun;
    total_exchangeable : dun;
    locked : bool
  }
  let storage_encode (*: storage Json_encoding.encoding*) =
    let open Json_encoding in
    fun {admin;swap_counter;swaps;swap_start;swap_end;swap_span;total_vouched;vested_vouched;swapped;vested_threshold;total_exchangeable;locked} -> (admin,swap_counter,swaps,swap_start,swap_end,swap_span,total_vouched,vested_vouched,swapped,vested_threshold,total_exchangeable,locked)
  let storage_decode (*: storage Json_encoding.encoding*) =
    let open Json_encoding in
    fun (admin,swap_counter,swaps,swap_start,swap_end,swap_span,total_vouched,vested_vouched,swapped,vested_threshold,total_exchangeable,locked) -> {admin;swap_counter;swaps;swap_start;swap_end;swap_span;total_vouched;vested_vouched;swapped;vested_threshold;total_exchangeable;locked}

  let storage_encoding  (*:  storage Json_encoding.encoding*) =
    let open Json_encoding in
    conv storage_encode storage_decode
    @@ obj1(req "record" @@
            obj12
              (req "admin" address_encoding)
              (req "swap_counter" nat_encoding)
              (req "swaps" (lovebigmap_encoding (nat_encoding)  (swap_t_encoding)))
              (req "swap_start" timestamp_encoding)
              (req "swap_end" timestamp_encoding)
              (req "swap_span" int_encoding)
              (req "total_vouched" dun_encoding)
              (req "vested_vouched" dun_encoding)
              (req "swapped" dun_encoding)
              (req "vested_threshold" dun_encoding)
              (req "total_exchangeable" dun_encoding)
              (req "locked" bool_encoding))
  let has_storage = true

  let get_storage ?(block="head") kt1 =
    R.get
      I.node_url
      (dune_expr @@ storage_encoding)
      (Printf.sprintf "chains/main/blocks/%s/context/contracts/%s/storage" block kt1 )


  let storage_example  () =
    {admin = (address_example ())
    ;swap_counter = (nat_example ())
    ;swaps = ((lovebigmap_example (nat_example)  (swap_t_example)) ())
    ;swap_start = (timestamp_example ())
    ;swap_end = (timestamp_example ())
    ;swap_span = (int_example ())
    ;total_vouched = (dun_example ())
    ;vested_vouched = (dun_example ())
    ;swapped = (dun_example ())
    ;vested_threshold = (dun_example ())
    ;total_exchangeable = (dun_example ())
    ;locked = (bool_example ())
    }

  let __init_storage_parameter_enc = (lovetup6 address_encoding timestamp_encoding timestamp_encoding int_encoding dun_encoding dun_encoding)
  let __init_storage ~src ?(amount=0L) (address_0 : address) (timestamp_1 : timestamp) (timestamp_2 : timestamp) (int_3 : int) (dun_4 : dun) (dun_5 : dun) =
    R.output_debug "Calling entrypoint __init_storage";
    begin
      R.originate ~src
        ~amount
        ~code:mycode
        ~parameter:(R.to_string
                      __init_storage_parameter_enc (address_0,timestamp_1,timestamp_2,int_3,dun_4,dun_5))

    end
  let deposit_parameter_enc = (lovetup5 bytes_encoding keyhash_encoding string_encoding string_encoding (loveoption_encoding string_encoding))
  let deposit_entry ~kt1 ~src ?(amount=0L) (bytes_0 : bytes) (keyhash_1 : keyhash) (string_2 : string) (string_3 : string) (option_4 : string option) =
    R.output_debug "Calling entrypoint deposit";
    begin
      R.send ~src ~destination:kt1
        ~entrypoint:"deposit"
        ~amount
        ~parameter:(R.to_string
                      deposit_parameter_enc (bytes_0,keyhash_1,string_2,string_3,option_4))

    end
  let swap_parameter_enc = (lovetup2 nat_encoding bytes_encoding)
  let swap_entry ~kt1 ~src ?(amount=0L) (nat_0 : nat) (bytes_1 : bytes) =
    R.output_debug "Calling entrypoint swap";
    begin
      R.send ~src ~destination:kt1
        ~entrypoint:"swap"
        ~amount
        ~parameter:(R.to_string
                      swap_parameter_enc (nat_0,bytes_1))

    end
  let refund_parameter_enc = (lovetup1 nat_encoding)
  let refund_entry ~kt1 ~src ?(amount=0L) (nat_0 : nat) =
    R.output_debug "Calling entrypoint refund";
    begin
      R.send ~src ~destination:kt1
        ~entrypoint:"refund"
        ~amount
        ~parameter:(R.to_string
                      refund_parameter_enc (nat_0))

    end
  let set_delegate_parameter_enc = (lovetup1 (loveoption_encoding keyhash_encoding))
  let set_delegate_entry ~kt1 ~src ?(amount=0L) (option_0 : keyhash option) =
    R.output_debug "Calling entrypoint set_delegate";
    begin
      R.send ~src ~destination:kt1
        ~entrypoint:"set_delegate"
        ~amount
        ~parameter:(R.to_string
                      set_delegate_parameter_enc (option_0))

    end
  let set_locked_parameter_enc = (lovetup1 bool_encoding)
  let set_locked_entry ~kt1 ~src ?(amount=0L) (bool_0 : bool) =
    R.output_debug "Calling entrypoint set_locked";
    begin
      R.send ~src ~destination:kt1
        ~entrypoint:"set_locked"
        ~amount
        ~parameter:(R.to_string
                      set_locked_parameter_enc (bool_0))

    end

end
