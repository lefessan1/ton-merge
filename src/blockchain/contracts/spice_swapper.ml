module R = Request_implementation.R

type spice_operation = R.spice_operation = {
  src : string;
  op :
    (Metal.Types.notif_manager_info,
     (Metal.Dune.Types.account_hash *
      (Bigstring.t -> (Bigstring.t, Metal.Misc.metal_error) result Lwt.t))
       option Metal_types.manager_details)
      Metal.Types.manager_operation;
}
module Metal = Request_implementation.Node
module Json_utils = Json_utils

module Contracts = struct

  module type N = sig val node_url : R.url end

  module I(N:N) = struct
    type url = R.url
    type address = string
    let node_url = N.node_url
    type contract_id = string
    let get_kt1 _ = assert false
    let set_kt1 _ _ = assert false
  end


  module Swap(N:N) = Contract_swap.Interface (R)(I(N))
  module Swap2(N:N) = Contract_swap2.Interface (R)(I(N))

end

module Code = struct
  module Swap = Swap_code
  module Swap2 = Swap2_code
end
