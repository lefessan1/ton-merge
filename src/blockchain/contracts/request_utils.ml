open Json_encoding

module type REQUEST = sig
  type url
  type ('a,'b) post_request
  type 'a get_request
  val output_debug : string -> unit
  val post : url -> 'a Json_encoding.encoding ->
             'b Json_encoding.encoding ->
             string ->
             'a -> ('a,'b) post_request
  val get : url -> 'a Json_encoding.encoding
            -> string -> 'a get_request
  type send_prerequest
  type originate_prerequest
  val send : src:string ->
             destination:string ->
             amount:int64 ->
             parameter:string ->
             entrypoint:string ->
             send_prerequest
  val originate : src_lang:string ->
                  src:string ->
                  amount:int64 ->
                  code:string ->
                  parameter:string ->
                  originate_prerequest
  val bigmap_getter : url -> ('a,'b) Json_utils.bigmap -> 'a encoding -> 'b encoding -> 'a -> ('a,'b option) post_request
  val to_string : 'a Json_encoding.encoding -> 'a -> string
end

module type INFO =
  sig
    type url
    type address = string
    val node_url : url
    type contract_id = string
    val get_kt1 : contract_id -> address
    val set_kt1 : contract_id -> address -> unit
  end
