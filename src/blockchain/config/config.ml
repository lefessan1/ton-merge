open Lwt_utils
open Db_common

type t = {
  nodes : EzAPI.base_url list;
  block_time : int;
  first_level : int option;
  forward : int option;
  confirmations : int32;
  verbose : int;
  crawler_sleep : float;
  api_port : int;
}

let add_end (EzAPI.TYPES.BASE url) =
  let urllen = String.length url in
  let url =
    Printf.sprintf "%s%s" url
      (if urllen = 0 || url.[urllen - 1] = '/' then "" else "/")
  in
  EzAPI.TYPES.BASE url

let encoding =
  Encoding_common.conv_ignore_extra
    (fun {nodes; block_time; first_level; forward; confirmations;
          verbose; crawler_sleep; api_port } ->
      (List.map (fun (EzAPI.TYPES.BASE u) -> u) nodes,
       block_time, first_level, forward, confirmations,
       verbose, crawler_sleep, api_port))
    (fun (nodes, block_time, first_level, forward, confirmations,
          verbose, crawler_sleep, api_port) ->
       { nodes = List.map (fun u -> add_end (EzAPI.TYPES.BASE u)) nodes;
         block_time; first_level; forward; confirmations;
         verbose; crawler_sleep; api_port }) @@
  let open Json_encoding in
  obj8
    (req "nodes" (list string))
    (dft ~construct:true "block_time" int 60)
    (opt "first_level" int)
    (opt "forward" int)
    (dft ~construct:true "confirmations" int32 10l)
    (dft "verbose" int 0)
    (dft ~construct:true "crawler_sleep" float 5.)
    (dft ~construct:true "api_port" int 8080)

let read_from_file f =
  let in_ch = open_in f in
  let json = Ezjsonm.from_channel in_ch in
  close_in in_ch;
  try
    Json_encoding.destruct encoding json
  with exn ->
    Format.eprintf "Could not read config file %s: @[<hv>%a@]@."
      f (Json_encoding.print_error ?print_unknown:None) exn;
    raise exn

let write_to_file c f =
  let out_ch = open_out f in
  let json = Json_encoding.construct encoding c in
  Ezjsonm.value_to_string ~minify:false json
  |> output_string out_ch;
  close_out out_ch


let get_config_option key encoding =
  let>>> dbh = () in
  [%pgsql dbh
      "select value_json  from blockchain_config where key = $key"
  ] >|= function
  | [] -> None
  | v :: _ -> Some (Json_encoding.destruct encoding v)

let set_config_option key value encoding =
  let value = Json_encoding.construct encoding value in
  let>>> dbh = () in
  [%pgsql dbh
      "insert into blockchain_config (key, value_json) values ($key, $value) \
       on conflict (key) do update set value_json = $value"
  ]

let get_nodes () =
  get_config_option "nodes" Json_encoding.(list string) >|= function
  | None -> failwith "No nodes configured"
  | Some l -> List.map (fun u -> add_end (EzAPI.TYPES.BASE u)) l

let get_block_time () =
  get_config_option "block_time" Json_encoding.int
  >|= Option.value ~default:60

let get_first_level () =
  get_config_option "first_level" Json_encoding.int

let get_forward () =
  get_config_option "forward" Json_encoding.int

let get_confirmations () =
  get_config_option "confirmations" Json_encoding.int32
  >|= Option.value ~default:10l

let get_verbose () =
  get_config_option "verbose" Json_encoding.int
  >|= Option.value ~default:0

let get_crawler_sleep () =
  get_config_option "crawler_sleep" Json_encoding.float
  >|= Option.value ~default:5.

let get_api_port () =
  get_config_option "api_port" Json_encoding.int
  >|= Option.value ~default:8080

let read_from_db () =
  let> nodes = get_nodes () in
  let> block_time =  get_block_time () in
  let> first_level = get_first_level () in
  let> forward = get_forward () in
  let> confirmations = get_confirmations () in
  let> crawler_sleep = get_crawler_sleep () in
  let> verbose = get_verbose () in
  let>! api_port = get_api_port () in
  { nodes ; block_time ; first_level ; forward ; confirmations ;
    verbose ; crawler_sleep; api_port }

let set_first_level l =
  set_config_option "first_level" l Json_encoding.int

let write_to_db c =
  let open Json_encoding in
  let> () =
    set_config_option "nodes"
      (List.map (fun (EzAPI.TYPES.BASE u) -> u) c.nodes)
      (list string) in
  let> () = set_config_option "block_time" c.block_time int in
  let> () = match c.first_level with
    | None -> Lwt.return_unit
    | Some l -> set_config_option "first_level" l int in
  let> () = match c.forward with
    | None -> Lwt.return_unit
    | Some f -> set_config_option "forward" f int in
  let> () =
    set_config_option "confirmations" c.confirmations int32 in
  let> () = set_config_option "crawler_sleep" c.crawler_sleep float in
  let> () = set_config_option "api_port" c.api_port int in
  let>! () = set_config_option "verbose" c.verbose int in
  ()

let to_string c =
  let json = Json_encoding.construct encoding c in
  Ezjsonm.value_to_string ~minify:false json
