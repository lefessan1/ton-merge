open Encoding_common
open Encoding
open EzAPI

module Encoding = struct
  include Encoding_common
  include Encoding
end

module Params = struct
  let origin = Param.string ~required:true ~descr:"Origin of swap" "origin"
  let confirmed =
    Param.bool ~required:false ~descr:"filter on confirmation status" "confirmed"
  let status =
    Param.string ~required:false ~descr:"filter on operation status" "status"
      ~examples:["queued"; "pending"; "unconfirmed"; "confirmed"; "error"]
  let kind =
    Param.string ~required:false ~descr:"filter on operation kind" "kind"
      ~examples:operation_kinds
end

let misc_section = Doc.section "Misc Requests"
let swaps_section = Doc.section "Swaps Requests"
let sections = [
  misc_section;
  swaps_section;
]

let server_info : (_, string option, no_security) service0 =
  service
    ~section:misc_section
    ~name:"Server Info"
    ~descr:"Show various information about server, like the version \
            of the database, the software revision and date, etc."
    ~output:server_info
    Path.(root // "server_info")

let list_swaps : (_, string option, no_security) service0 =
  service
    ~section:swaps_section
    ~name:"List swaps by origin"
    ~descr:"List all swaps by origin"
    ~params:[Params.origin]
    ~output:(Json_encoding.list swap)
    Path.(root // "list_swaps")

let list_operations : (_, string option, no_security) service0 =
  service
    ~section:swaps_section
    ~name:"List operations by origin"
    ~descr:"List all operations by origin"
    ~params:[Params.origin]
    ~output:Json_encoding.(list (operation (option block)))
    Path.(root // "list_operations")

let swapper_info : (_, string option, no_security) service0 =
  service
    ~section:swaps_section
    ~name:"Get swapper info"
    ~descr:"Return swapper info and current state"
    ~output:swapper_info
    Path.(root // "swapper_info")
