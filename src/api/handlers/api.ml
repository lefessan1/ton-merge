
module S = Services
module H = Handlers

let () = Random.self_init ()

let catch exn =
  let id = Random.int32 Int32.max_int in
  Format.eprintf "Unhandled exception [Error id: %ld]: @[%s@]@."
    id
    (Printexc.to_string exn);
  let err_kind, msg, code = match exn with
    | Failure s -> "failure", s, 500
    | Invalid_argument s -> "invalid_argument", s, 400
    | _ -> "unhandled_error", "Contact administrator", 500 in
  let json = `O [
      "error", `String err_kind;
      "error_id", `Float (Int32.to_float id);
      "message", `String msg
    ] in
  code, json

let register ( s : (
    EzAPI.Req.t, unit,
    'a, string option, EzAPI.no_security) EzAPI.service) h dir =
  let h a _ b =
    Lwt.catch (fun () -> h a b) (fun e ->
        let code, json = catch e in
        EzAPIServerUtils.return_error
          ~content:(Ezjsonm.value_to_string json) code
      ) in
  EzAPIServerUtils.register s h dir

let services =
  EzAPIServerUtils.empty
  |> register S.server_info H.server_info
  |> register S.list_swaps H.list_swaps
  |> register S.list_operations H.list_operations
  |> register S.swapper_info H.swapper_info
