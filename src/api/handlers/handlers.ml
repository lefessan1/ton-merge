open Lwt_utils
open Data_types

module Params = struct

  let origin params =
    match EzAPI.Req.find_param Services.Params.origin params with
    | None -> invalid_arg "Missing parameter origin"
    | Some s -> s

  let confirmed params =
    match EzAPI.Req.find_param Services.Params.confirmed params with
    | None -> None
    | Some b ->
      try Some (bool_of_string b)
      with _ ->
        Format.ksprintf invalid_arg
          "confirmed parameter %s is not a valid boolean" b

  let status params =
    match EzAPI.Req.find_param Services.Params.status params with
    | None -> None
    | Some s ->
      try Some (Db.op_status_of_string s)
      with _ ->
        Format.ksprintf invalid_arg
          "status parameter %s is not a valid status (valid statuses: %s)" s
          (String.concat ", " ["queued"; "pending"; "unconfirmed"; "confirmed"; "error"])

  let kind params =
    match EzAPI.Req.find_param Services.Params.kind params with
    | None -> None
    | Some s ->
      if List.mem s Encoding_common.operation_kinds then
        Some s
      else
        Format.ksprintf invalid_arg
          "kind parameter %s is not a valid operation kind (valid kinds: %s)" s
          (String.concat ", " Encoding_common.operation_kinds)
end

let to_api_result p =
  Lwt.bind p @@ function
  | Error e -> EzAPIServerUtils.return ~code:500 (Error e)
  | Ok x -> EzAPIServerUtils.return (Ok x)

let to_api p =
  Lwt.bind p @@ fun x -> EzAPIServerUtils.return (Ok x)


let server_info _params () = to_api @@
  let>! v_db_version = Db.get_version () in
  let db_info = {
    v_db = Project_config.database;
    v_db_version
  } in
  object
    method db_info = db_info
    method software_revision = Git_info.abbreviated_commit_hash
    method software_date = Git_info.committer_date
  end

let list_swaps params () = to_api @@
  let origin = Params.origin params in
  Db.SWAPS.get_swaps_by_origin origin

let list_operations params () = to_api @@
  let origin = Params.origin params in
  let confirmed = Params.confirmed params in
  let status = Params.status params in
  let kind = Params.kind params in
  Db.list_operations ?confirmed ?status ~origin ?kind ()

let swapper_info _params () = to_api @@
  let>! opt_info = Db.get_swapper_info () in
  match opt_info with
  | None -> raise Not_found
  | Some s -> s
