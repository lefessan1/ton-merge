type timestamp = CalendarLib.Calendar.t

type version = {
  v_db: string;
  v_db_version: int;
}

type op_status =
  | OpQueued
  | OpPending
  | OpUnconfirmed
  | OpConfirmed
  | OpError

type block = {
  block_hash : string;
  block_predecessor : string option;
  block_level : int32;
  block_timestamp : timestamp;
}

type 'block full_status =
  | Queued
  | Pending of {
      operation_hash : string;
      index : int;
      internal_index : int;
    }
  | Unconfirmed of {
      operation_hash : string;
      index : int;
      internal_index : int;
      block : 'block;
      confirmations : int;
    }
  | Confirmed of  {
      operation_hash : string;
      index : int;
      internal_index : int;
      block : 'block;
    }
  | Errored of {
      err_kind : [`Retry of int | `Permanent ];
      msg : string;
    }

type swap_dune_status =
  | SwapSubmitted    (* order submitted *)
  | SwapConfirmed    (* order submitted and confirmed *)
  | SwapCanReveal
  | SwapCompleted    (* DUN burnt after swap completed *)
  | SwapExpired      (* order expired on Free TON *)
  | SwapRefundAsked  (* refund asked *)
  | SwapRefunded     (* refund confirmed *)

type swap_freeton_status =
  | SwapWaitingForConfirmation
  | SwapFullyConfirmed
  | SwapWaitingForCredit
  | SwapCreditDenied
  | SwapCredited
  | SwapRevealed
  | SwapWaitingForDepool
  | SwapDepoolDenied
  | SwapTransferred
  | SwapCancelled

type operation_content =
  | Deposit of {
      hashed_secret : bytes;
      origin_address : Dune.Types.account_hash;
      amount : Z.t;
      return_address : Dune.Types.account_hash;
      freeton_address: string;
      freeton_pubkey: string;
      freeton_depool : string option;
      swap_id : int option;
    }
  | Swap of {
      swap_id : int;
      secret : bytes;
    }
  | Refund of {
      swap_id : int;
    }

type 'block operation = {
  op_id : int32;
  op_source : string;
  op_status : 'block full_status;
  op_content : operation_content;
}

type swap = {
  swap_id : int;
  dune_origin : Dune.Types.account_hash;
  dun_amount : Z.t;
  ton_amount : Z.t; (* in nanotons *)
  mutable dune_status : swap_dune_status;
  mutable freeton_status : swap_freeton_status;
  hashed_secret : bytes;
  time : timestamp;
  refund_address: Dune.Types.account_hash;
  freeton_pubkey: string;
  freeton_address: string;
  freeton_depool: string option;
  mutable secret : string option;
  confirmations : int;
  logical_time : int64;
}


type swapper_info = {
  swap_start : timestamp;
  swap_end : timestamp;
  swap_span : int;
  total_vouched : Z.t;
  vested_vouched : Z.t;
  swapped : Z.t;
  vested_threshold : Z.t;
  total_exchangeable : Z.t;
  locked : bool;
}

type freeton_config = {
  network_url : string ;
  relay_pubkey : string ;
  giver_address : string ;
  root_address : string ;
  event_address : string ;
  mutable last_blockid : string ;
}
