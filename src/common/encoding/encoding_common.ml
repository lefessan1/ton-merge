open Json_encoding
open Data_types

let conv_ignore_extra ffrom fto enc =
  let open Json_encoding in
  conv
    (fun x -> ffrom x, ())
    (fun (x, ()) -> fto x)
    (merge_objs enc unit (* = ignore *))

(* base *)
let uint32 = ranged_int32 ~minimum:0l ~maximum:Int32.max_int "uint32"
let uint53 = ranged_int53 ~minimum:0L ~maximum:(Int64.shift_left 1L 53) "uint53"
let uint31 = ranged_int ~minimum:0 ~maximum:((1 lsl 30) - 1) "uint31"
let posint31 = ranged_int ~minimum:1 ~maximum:((1 lsl 30) - 1) "posint31"
let tup1_int32_schema = Json_schema.(
    create @@ element @@
    Monomorphic_array (element (Integer {
        numeric_specs with
        minimum = Some (0., `Inclusive);
        maximum = Some (Int32.to_float Int32.max_int, `Inclusive)}
      ), {array_specs with min_items = 1; max_items = Some 1}))
let tup1_uint32 =
  conv (fun x -> x) (fun x -> x)
    ~schema:tup1_int32_schema @@
    tup1 uint32

let str_sch ?max_length ?(min_length=0) ?format ?pattern () = Json_schema.(
    create @@ element @@ String {min_length; pattern; max_length; str_format = format})

let sstring ?max_length ?min_length ?format ?pattern () =
  conv ~schema:(str_sch ?max_length ?min_length ?format ?pattern ())
    (fun s -> s) (fun s -> s) string

let tdef ?descr title enc = def ?description:descr ~title title enc

let dstring ?descr ?max_length ?min_length ?format ?pattern ?title s =
  let title = Option.value ~default:s title in
  match max_length, min_length, format, pattern with
  | None, None, None, None -> tdef ?descr title string
  | _ ->
    tdef ?descr title @@ sstring ?max_length ?min_length ?format ?pattern ()

let hex_bytes =
  conv
    (fun b -> Hex.show (Hex.of_bytes b))
    (fun s -> Hex.to_bytes (`Hex s))
    string

let timestamp_str = sstring ~format:"date-time" ()

let block_hash =
  dstring ~min_length:51 ~max_length:51 ~pattern:"B[A-Za-z0-9]{50}"
    ~title:"Block hash"
    ~descr:"Unique hash of a block in the blockchain in base58-check"
    "block_hash"

let operation_hash =
  dstring ~min_length:51 ~max_length:51 ~pattern:"o[A-Za-z0-9]{50}"
    ~title:"Operation hash"
    ~descr:"Hash of a batch of operations, in base58-check"
    "operation_hash"

let public_key_hash =
  dstring ~min_length:36 ~max_length:36 ~pattern:"dn(1|2|3)[A-Za-z0-9]{33}"
    ~title:"Public key hash"
    ~descr:"Public key hash in base58-check"
    "public_key_hash"

let z =
  def ~title:"unbounded integer" "unbounded_integer"
    ~description:"An unbounded integer represented by a string of digits" @@
  conv Z.to_string Z.of_string string


let string_of_pos_amount ~decimals a =
  let ten_dec = Z.pow (Z.of_int 10) decimals in
  let whole, rest = Z.div_rem a ten_dec in
  let rest_str = Z.to_string rest in
  if Z.equal rest Z.zero then Z.to_string whole
  else
    let l = String.length rest_str in
    let rest_str = String.make (decimals - l) '0' ^ rest_str in
    let rec pos_nz i =
      if i <= 0 then 0
      else if rest_str.[i-1] = '0' then pos_nz (i - 1)
      else i in
    let p = pos_nz decimals in
    if p = 0 then Z.to_string whole
    else
      let rest_str = String.sub rest_str 0 p in
      String.concat "." [Z.to_string whole; rest_str]

let amount_of_string ~decimals s =
  let ten_dec = Z.pow (Z.of_int 10) decimals in
  try
    (match String.index_opt s '/' with
     | Some _ -> raise Exit
     | None -> ()
    );
    (match String.rindex_opt s '.' with
     | None -> ()
     | Some p -> if String.length s - p - 1 > decimals then raise Exit
    );
    Q.of_string s
    |> Q.mul (Q.of_bigint ten_dec)
    |> Q.to_bigint
  with _ ->
    failwith
      (Printf.sprintf "Bad format for amount with at most %d decimals" decimals)

let string_of_amount ~decimals a =
  if Z.sign a >= 0 then
    string_of_pos_amount ~decimals a
  else
    "-" ^ string_of_pos_amount ~decimals @@ Z.neg a

let d_amount decimals =
  def ~title:"amount" "amount"
    ~description:(Printf.sprintf "Amount in full units with %d decimals" decimals) @@
  conv
    ~schema:(str_sch
               ~format:(Printf.sprintf "amount-%d" decimals)
               ~pattern:(Printf.sprintf "[0-9]+(.[0-9]{0,%d})?" decimals) ())
    (string_of_amount ~decimals)
    (amount_of_string ~decimals)
    string


let amount_of_string s = amount_of_string ~decimals:6 s
let string_of_amount a = string_of_amount ~decimals:6 a
let amount = d_amount 6

let cal_to_str ?(ms=false) date =
  let s = CalendarLib.Printer.Calendar.sprint "%Y-%m-%dT%H:%M:%S" date in
  if ms then s ^ ".000Z" else s ^ "Z"

let cal_of_str ?(ms=false) s =
  try
    float_of_string s
    |> CalendarLib.Calendar.from_unixfloat
  with _ ->
    let s =
      if ms then String.sub s 0 (String.length s - 5)
      else String.sub s 0 (String.length s - 1) in
    CalendarLib.Printer.Calendar.from_fstring "%Y-%m-%dT%H:%M:%S" s

let timestamp =
  conv (cal_to_str ~ms:false) (cal_of_str ~ms:false) timestamp_str

let timestamp_ms =
  conv (cal_to_str ~ms:true) (cal_of_str ~ms:true) timestamp_str

let any_ezjson_object =
  def "any_object" ~description:"Any JSON object" @@
  conv
    ~schema:Json_schema.(create @@ element @@ Object object_specs)
    (fun (`O _ as o) -> (o :> Json_repr.ezjsonm))
    (function
      | `O o -> `O o
      | _ -> failwith "Invalid JSON object for any_object"
    )
    any_ezjson_value

let dictionary =
  def "dictionary"
    ~description:"A dictionary JSON object \
                  (_i.e._ object with string values)" @@
  conv
    ~schema:Json_schema.(create @@ element @@
                         Object { object_specs with
                                  additional_properties = Some (
                                      element @@ String string_specs) })
    (fun l ->
       `O (List.map (fun (k, v) -> k, `String v) l))
    (function
      | `O l ->
        List.map (fun (k, o) -> match o with
            | `String v -> k ,v
            | _ -> failwith @@ Printf.sprintf
                "Invalid JSON dictionary (field %s must contain a string)"
                k
          ) l
      | _ -> failwith "Invalid JSON dictionary (is not an object)"
    )
    any_ezjson_value

let version = conv
  (fun {v_db; v_db_version} -> (v_db, v_db_version))
  (fun (v_db, v_db_version) -> {v_db; v_db_version}) @@
  obj2
    (req "db" string)
    (req "db_version" int)

let block =
  def
    "Block"
    ~description:"Description of a block in the blockchain" @@
  conv
    (fun { block_hash; block_predecessor; block_level; block_timestamp } ->
       (block_hash, block_predecessor, block_level, block_timestamp))
    (fun (block_hash, block_predecessor, block_level, block_timestamp) ->
       { block_hash; block_predecessor; block_level; block_timestamp }) @@
  obj4
    (req "hash" block_hash
       ~description:"Unique hash of the block")
    (opt "predecessor" block_hash
       ~description:"Hash of predecessor block in the chain")
    (req "level" int32
       ~description:"Level (i.e. number) of the block")
    (req "timestamp" timestamp
       ~description:"Timestamp of the block (when the block was mined)")

let err_kind =
  def "err_kind"
    ~description:"The kind of blockchain errors, either retryable of permanent" @@
  union [
    case
      (obj1 (req "err_kind" (constant "permanent")))
      (function `Permanent -> Some () | _ -> None)
      (function () -> `Permanent);
    case
      (obj2
         (req "err_kind" (constant "temporary"))
         (req "remaining_retries" int))
      (function `Retry i -> Some ((), i) | _ -> None)
      (function ((), i) -> `Retry i);
  ]

let full_status block =
  def
    "Operation status"
    ~description:"The status of a blockchain operation" @@
  union [
    case
      (def ~title:"queued" "queued_status" @@ obj1 (req "status" (constant "queued")))
      (function Queued -> Some () | _ -> None)
      (fun () -> Queued);

    case
      (def ~title:"pending" "pending_status" @@ obj4
         (req "status" (constant "pending"))
         (req "operation_hash" operation_hash
            ~description:"Hash of operation batch on the blockchain")
         (req "index" int
            ~description:"Index in operation batch")
         (req "internal_index" int
            ~description:"Index in internal operations list"))
      (function
        | Pending { operation_hash; index; internal_index } ->
          Some ((), operation_hash, index, internal_index)
        | _ -> None)
      (fun ((), operation_hash, index, internal_index) ->
         Pending { operation_hash; index; internal_index });

    case
      (def ~title:"unconfirmed" "unconfirmed_status" @@ obj6
         (req "status" (constant "unconfirmed"))
         (req "operation_hash" operation_hash
            ~description:"Hash of operation batch on the blockchain")
         (req "index" int
            ~description:"Index in operation batch")
         (req "internal_index" int
            ~description:"Index in internal operations list")
         (req "confirmations" int
            ~description:"Number of confirmations received \
                          by for this operation")
         (req "block" block
            ~description:"Block (of the blockchain) in which the operation \
                          was included"))
      (function
        | Unconfirmed { operation_hash; index; internal_index; block; confirmations } ->
          Some ((), operation_hash, index, internal_index, confirmations, block)
        | _ -> None)
      (fun ((), operation_hash, index, internal_index, confirmations, block) ->
         Unconfirmed { operation_hash; index; internal_index; block; confirmations });

    case
      (def ~title:"confirmed" "confirmed_status" @@ obj5
         (req "status" (constant "confirmed"))
         (req "operation_hash" operation_hash
            ~description:"Hash of operation batch on the blockchain")
         (req "index" int
            ~description:"Index in operation batch")
         (req "internal_index" int
            ~description:"Index in internal operations list")
         (req "block" block
            ~description:"Block (of the blockchain) in which the operation \
                          was included"))
      (function
        | Confirmed { operation_hash; index; internal_index; block } ->
          Some ((), operation_hash, index, internal_index, block)
        | _ -> None)
      (fun ((), operation_hash, index, internal_index, block) ->
         Confirmed { operation_hash; index; internal_index; block });

    case
      (def ~title:"error operation" "error_status" @@
       merge_objs
         (obj2
            (req "status" (constant "error"))
            (req "error_message" string))
         err_kind)
      (function
        | Errored { msg; err_kind } -> Some (((), msg), err_kind)
        | _ -> None)
      (fun (((), msg), err_kind) -> Errored { msg; err_kind });
  ]

let operation_kinds = ref []

let op_case kind enc constr destr =
  operation_kinds := kind :: !operation_kinds;
  case
    (def (kind ^ "_operation") @@ merge_objs
       (obj1 (req "kind" (constant kind)))
       enc)
    (fun op -> match constr op with
       | None -> None
       | Some x -> Some ((), x))
    (fun ((), x) -> destr x)

let operation_content =
  union [
    op_case "deposit"
      (obj8
         (req "hashed_secret" hex_bytes
            ~description:"SHA256 hash of secret in hexadecimal")
         (req "origin_address" string
            ~description:"Address making the swap Dune Network")
         (req "amount" amount
            ~description:"Amount being swapped")
         (req "return_address" string
            ~description:"Return address on Dune Network for refund")
         (req "freeton_address" string
            ~description:"Address to credit on Free TON")
         (req "freeton_pubkey" string
            ~description:"Public key for swap receiver on Free TON (who controls freeton_address) ")
         (opt "freeton_depool" string
            ~description:"Address of Depool on Free TON to vest funds if any")
         (opt "swap_id" int
            ~description:"Id of swap created (to call entry point swap on)")
      )
      (function
        | Deposit {hashed_secret; origin_address; amount;
                   return_address; freeton_address; freeton_pubkey; freeton_depool; swap_id} ->
          Some (hashed_secret, origin_address, amount,
                return_address, freeton_address, freeton_pubkey, freeton_depool, swap_id)
        | _ -> None)
      (fun (hashed_secret, origin_address, amount,
            return_address, freeton_address, freeton_pubkey, freeton_depool, swap_id) ->
        Deposit {hashed_secret; origin_address; amount;
                 return_address; freeton_address; freeton_pubkey;
                 freeton_depool; swap_id});

    op_case "swap"
      (obj2
         (req "swap_id" int
            ~description:"Id of swap created (to call entry point swap on)")
         (req "secret" bytes
            ~description:"Revealed secret for swap")
      )
      (function
        | Swap {swap_id; secret} -> Some (swap_id, secret)
        | _ -> None)
      (fun (swap_id, secret) -> Swap {swap_id; secret});

    op_case "refund"
      (obj1
         (req "swap_id" int
            ~description:"Id of swap created (to call entry point swap on)")
      )
      (function
        | Refund {swap_id} -> Some (swap_id)
        | _ -> None)
      (fun (swap_id) -> Refund {swap_id});
  ]

let operation_kinds = !operation_kinds

let dune_status_of_string = function
  | "submitted" -> SwapSubmitted
  | "confirmed" -> SwapConfirmed
  | "can_reveal" -> SwapCanReveal
  | "completed" -> SwapCompleted
  | "expired" -> SwapExpired
  | "refund_asked" -> SwapRefundAsked
  | "refunded" -> SwapRefunded
  | _ -> invalid_arg "dune status"
let string_of_dune_status = function
  | SwapSubmitted -> "submitted"
  | SwapConfirmed -> "confirmed"
  | SwapCanReveal -> "can_reveal"
  | SwapCompleted -> "completed"
  | SwapExpired -> "expired"
  | SwapRefundAsked -> "refund_asked"
  | SwapRefunded -> "refunded"

let freeton_status_of_string = function
  | "SwapWaitingForConfirmation" -> SwapWaitingForConfirmation
  | "SwapFullyConfirmed" -> SwapFullyConfirmed
  | "SwapWaitingForCredit" -> SwapWaitingForCredit
  | "SwapCreditDenied" -> SwapCreditDenied
  | "SwapCredited" -> SwapCredited
  | "SwapRevealed" -> SwapRevealed
  | "SwapWaitingForDepool" -> SwapWaitingForDepool
  | "SwapDepoolDenied" -> SwapDepoolDenied
  | "SwapTransferred" -> SwapTransferred
  | "SwapCancelled" -> SwapCancelled

  | _ -> invalid_arg "freeton status"
let string_of_freeton_status = function
  | SwapWaitingForConfirmation -> "SwapWaitingForConfirmation"
  | SwapFullyConfirmed -> "SwapFullyConfirmed"
  | SwapWaitingForCredit -> "SwapWaitingForCredit"
  | SwapCreditDenied -> "SwapCreditDenied"
  | SwapCredited -> "SwapCredited"
  | SwapRevealed -> "SwapRevealed"
  | SwapWaitingForDepool -> "SwapWaitingForDepool"
  | SwapDepoolDenied -> "SwapDepoolDenied"
  | SwapTransferred -> "SwapTransferred"
  | SwapCancelled -> "SwapCancelled"


let dune_status =
  conv string_of_dune_status dune_status_of_string string

let freeton_status =
  conv string_of_freeton_status freeton_status_of_string string

let operation block =
  def ~title:"operation" "operation"
    ~description:"An blockchain operation with its current status" @@
  conv
    (fun {op_id; op_source; op_status; op_content } ->
      ((op_id, op_source, op_content), op_status ))
    (fun ((op_id, op_source, op_content), op_status) ->
      {op_id; op_source; op_status; op_content }) @@
  merge_objs
    (obj3
       (req "id" uint32
          ~description:"Unique id of blockchain operation")
       (req "source" public_key_hash
          ~description:"Source which emitted the opertaion on the blockchain")
       (req "content" operation_content
          ~description:"Actual content of blockchain operation")
    )
    (full_status block)


let swap =
  def ~title:"swap" "swap"
    ~description:"A swap between Dune Network and Free TON" @@
  conv
    (fun s ->
       (
         (s.swap_id, s.dune_origin, s.dun_amount, s.ton_amount,
          s.dune_status, s.freeton_status,
          s.hashed_secret, s.time, s.refund_address, s.freeton_pubkey ),
         ( s.freeton_address, s.freeton_depool, s.secret, s.confirmations)) )
    (fun (
       ( swap_id, dune_origin, dun_amount, ton_amount,
         dune_status, freeton_status,
         hashed_secret, time, refund_address, freeton_pubkey ),
       ( freeton_address, freeton_depool, secret, confirmations )) ->
       {swap_id; dune_origin; dun_amount ; ton_amount;
        dune_status; freeton_status;
        hashed_secret; time; refund_address; freeton_pubkey;
        freeton_address; freeton_depool; secret; confirmations ;
        logical_time = 0L })
  @@
  (merge_objs
     (obj10
        (req "swap_id" int
           ~description:"Id of swap created (to call entry point swap on)")
        (req "dune_origin" string
           ~description:"Address making the swap Dune Network")
        (req "dun_amount" amount
           ~description:"Amount being swapped")
        (req "ton_amount" amount
           ~description:"Amount being swapped")
        (req "status" dune_status
           ~description:"Current status of swap")
        (dft "freeton_status" freeton_status SwapWaitingForConfirmation
           ~description:"Current status of swap on FreeTON")
        (req "hashed_secret" hex_bytes
           ~description:"SHA256 hash of secret in hexadecimal")
        (req "time" timestamp
           ~description:"Time when swapped last changed status")
        (req "refund_address" string
           ~description:"Address on Dune Network for refund")
        (req "freeton_pubkey" string
           ~description:"Pubkey of user on Free TON"))
     (obj4
        (req "freeton_address" string
           ~description:"Address to credit on Free TON")
        (opt "freeton_depool" string
           ~description:"Address of Depool on Free TON if vesting")
        (opt "secret" string
           ~description:"Secret observed on Free TON")
        (dft "confirmations" int 0
           ~description:"Secret observed on Free TON"))
  )

let swapper_info =
  def ~title:"swapper_info" "swapper_indo"
    ~description:"Information and current state of swapper" @@
  conv_ignore_extra
    (fun {swap_start; swap_end; swap_span;
          total_vouched; vested_vouched;
          swapped; vested_threshold; total_exchangeable; locked} ->
      (swap_start, swap_end, swap_span,
       total_vouched, vested_vouched,
       swapped, vested_threshold, total_exchangeable, locked))
    (fun (swap_start, swap_end, swap_span,
          total_vouched, vested_vouched,
          swapped, vested_threshold, total_exchangeable, locked) ->
      {swap_start; swap_end; swap_span;
       total_vouched; vested_vouched;
       swapped; vested_threshold; total_exchangeable; locked})
  @@
  (obj9
     (req "swap_start" timestamp
        ~description:"Timestamp of start of swap period")
     (req "swap_end" timestamp
        ~description:"Timestamp of end of swap period")
     (req "swap_span" int
        ~description:"Timespan of individual swaps, in seconds")
     (req "total_vouched" amount
        ~description:"Total amount of tokens vouched/deposited for swap")
     (req "vested_vouched" amount
        ~description:"Total amount of vested tokens vouched/deposited for swap")
     (req "swapped" amount
        ~description:"Total amount of tokens already swapped")
     (req "vested_threshold" amount
        ~description:"Threshold amount of swapped vested_tokens for the swap to be effective")
     (req "total_exchangeable" amount
        ~description:"Total number of token that can be exchanged")
     (dft ~construct:true "locked" bool false
        ~description:"true if contract is locked, cannot call refund anymore")
  )
