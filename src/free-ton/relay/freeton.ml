
let testnet_url = "https://net.ton.dev"
let mainnet_url = "https://main.ton.dev"
let tonos_url = "http://0.0.0.0:7081"

let abi_DuneGiver = match Ton_merge_contracts.read "DuneGiver.abi.json" with
  | None -> assert false
  | Some abi -> abi

let tvc_DuneUserSwap = match Ton_merge_contracts.read "DuneUserSwap.tvm" with
  | None -> assert false
  | Some abi -> abi

let abi_DuneUserSwap = match Ton_merge_contracts.read "DuneUserSwap.abi.json" with
  | None -> assert false
  | Some abi -> abi

let abi_DuneEvents = match Ton_merge_contracts.read "DuneEvents.abi.json" with
  | None -> assert false
  | Some abi -> abi

let abi_DuneRootSwap = match Ton_merge_contracts.read "DuneRootSwap.abi.json" with
  | None -> assert false
  | Some abi -> abi

let keypair = match Sys.getenv "TON_MERGE_PASSPHRASE" with
  | exception Not_found -> None
  | passphrase ->
    Some ( Ton_sdk.CRYPTO.generate_keypair_from_mnemonic passphrase )

let addr_zero =
  "0:0000000000000000000000000000000000000000000000000000000000000000"



module TYPES = struct

  (* as returned by the 'get' method of DuneGiver *)
  type giver_config = {
    giver_address : string ;
    root_address : string ;
    event_address : string ;
    expiration_date : string ;
    freeton_giver : string ;
  } [@@deriving json_encoding]

end

let json_of_string s = Hex.show (Hex.of_string s)
let string_of_json s = Hex.to_string ( `Hex s )
let json_of_swap_id swap_id = json_of_string (string_of_int swap_id)
let swap_id_of_json order_id = int_of_string ( string_of_json order_id )

let json_of_pubkey pubkey = "0x" ^ pubkey
let pubkey_of_json pubkey =
  assert ( pubkey.[0] = '0' && pubkey.[1] = 'x' );
  let len = String.length pubkey in
  String.sub pubkey 2 (len-2)

(* THIS CODE IS DUPLICATED in crawler_helper.ml *)
(* ton_amount (nano) = ( dun_amount (mu) * 1000 ) * 10 / 503 *)
let dun_ton_mul = Z.of_string "10_000"
let dun_ton_div = Z.of_string "503"
let ton_of_dun dun_amount =
  Z.div ( Z.mul dun_amount dun_ton_mul ) dun_ton_div
