open Lwt_utils

let string_of_swap s =
  EzEncoding.construct ~compact:true Encoding_common.swap s

let check_pid s =
  let name = s ^ "_pid" in
  let> old_pid = Db.CONFIG.read ~name in
  let old_pid = int_of_string old_pid in
  match Unix.kill old_pid 0 with
  | exception exn ->
    Printf.eprintf "former %s with pid %d is dead (exn %s)\n%!"
      s old_pid ( Printexc.to_string exn );
    let value = string_of_int @@ Unix.getpid () in
    let> () = Db.CONFIG.update ~name ~value in
    Lwt.return_unit
  | _ ->
    Printf.eprintf "%s is already running with pid %d\n%!" s old_pid;
    exit 2
