open Lwt_utils
open Data_types


let config_f = ref ""
let force = ref false

let set_config ~giver_address ~network_url =
  let client = Ton_sdk.CLIENT.create network_url in
  let> result =
    Ton_sdk.ACTION.call_lwt ~client ~server_url:network_url
      ~address:giver_address ~abi:Freeton.abi_DuneGiver
      ~meth:"get" ~params:"{}" ~local:true
      ?keypair:Freeton.keypair ()
  in
  match result with
  | Error exn ->
    Printf.eprintf "set_config: exception %s\n%!" (Printexc.to_string exn);
    exit 2
  | Ok result ->
    let {
      Freeton.TYPES.root_address ;
      giver_address = _ ;
      event_address ;
      expiration_date = _ ;
      freeton_giver = _ ;
    } = EzEncoding.destruct Freeton.TYPES.giver_config_enc result in
  let blockid = Ton_sdk.BLOCK.find_last_shard_block ~client
      ~address:event_address in
    let config = {
      Data_types.network_url ;
      giver_address ;
      root_address ;
      event_address ;
      last_blockid = blockid ;
      relay_pubkey = ( match Freeton.keypair with
          | None ->
            Printf.eprintf "TON_MERGE_PASSPHRASE must be defined\n%!";
            exit 2
          | Some key_pair -> key_pair.public);
    } in
    let> () = Db.CONFIG.set config in
    Lwt.return config

let get_config () =
  let> config = Db.CONFIG.get () in
  match config with
  | Some config ->
    Printf.eprintf "Current config: \n%!";
    Printf.eprintf "   network_url  : %s\n%!" config.network_url ;
    Printf.eprintf "   relay_pubkey : %s\n%!" config.relay_pubkey ;
    Printf.eprintf "   giver_address: %s\n%!" config.giver_address ;
    Printf.eprintf "   root_address : %s\n%!" config.root_address ;
    Printf.eprintf "   event_address: %s\n%!" config.event_address ;
    Printf.eprintf "   last_blockid : %s\n%!" config.last_blockid ;

    Lwt.return config
  | None ->
    Printf.eprintf "No config. You must call with --giver GIVER.\n%!";
    exit 2


let get_config ~giver_address ~network_url =
  match giver_address, network_url with
  | None, Some _ ->
    Printf.eprintf "You must specify --giver GIVER with --network\n%!";
    exit 2
  | Some giver_address, _ ->
    set_config ~giver_address
      ~network_url:(match network_url with
          | None -> Freeton.tonos_url
          | Some url -> url)
  | None, None -> get_config ()


(* These are tests using dune-ix and ft configs for sandboxing.
   swap_100 is a swap of dune-ix user0 to fr user0 of 10 DUN *)
let test_swaps =
  let secret = "Bonjour" in
  let hashed_secret =
    Hex.to_bytes
      (`Hex
         "9172e8eec99f144f72eca9a568759580edadb2cfd154857f07e657569493bc44")
  in
  let swap =     {
      swap_id = 0;
      dune_origin = "";
      dun_amount = Z.of_string "90_000_000";
      ton_amount = Z.zero; (* not used *)
      dune_status = SwapSubmitted ;
      freeton_status = SwapWaitingForConfirmation;
      hashed_secret ;
      time = CalendarLib.Calendar.from_unixfloat 1000. ; (* not used *)
      refund_address =  "";
      freeton_pubkey = "";
      freeton_address = "";
      freeton_depool = None;
      secret = Some secret ;
      confirmations = 0 ;
      logical_time = 0L ;
    }
  in
  let swap_100 =
    { swap with
      swap_id = 100;
      dune_origin = "dn1QudFDa1k15xwnXWYxgMRJf4RGJV1yzDym";
      dun_amount = Z.of_string "10_000_000_000";
      freeton_pubkey = "fd9ee2babfa35b65917f732316dbb3d31935ccacd2a0aa92e043f8c762e0da28";
      freeton_address = "0:108f6113fb0cad8c98b70e8ea3cfd12b52710ec20441d05ceb78cacb4f5566b7";
    }
  in
  let swap_101 =
    { swap with
      swap_id = 101;
      dune_origin = "dn1YJFYFw6ER8GFct3JA6ktRT1pjynV1juCy";
      dun_amount = Z.of_string "20_000_000_000";
      freeton_pubkey = "0857aea8cc2c96b955095ee2ceaf71f6708db009caa720ca8cd962ca1aecb4f2";
      freeton_address = "0:f89872394a383dc289f27ded48f02a6269e19d02be5821ba8081c67a1070588a";
    }
  in
  let swap_102 =
    { swap with
      swap_id = 102;
      dune_origin = "dn1KhdkBRSRdPUVtXZaz9kNMX3nxd92LVBwH";
      dun_amount = Z.of_string "30_000_000_000";
      freeton_pubkey = "d183edabfa08d900dd1b177948351817771cdd57ac9ff8576925ad76856689eb";
      freeton_address = "0:c0788ae1d2f2e698ef95cfe9c2221e1f334d69779bfdbd5c1bf22aa95eb91f4f";
    }
  in
  let swap_103 =
    { swap with
      swap_id = 103;
      dune_origin = "dn1HEggbbpWYovvKzpnBfpSN82fip56fk5K2";
      dun_amount = Z.of_string "40_000_000_000";
      freeton_pubkey = "e950bca31326f661144c918020a10bfd6e1f3a6bd16dba504856c5983ae1363f";
      freeton_address = "0:e4fe3af7e0afc23fb1209dd956dd74cfaf7b805b16d6809047de7aa9ad65c354";
    }
  in
  [|
    swap_100 ;
    swap_101 ;
    swap_102 ;
    swap_103 ;
  |]

(*
 ( "user4",
      tez 100,
      ( "dn1ZFsha5jeedydCiUxj3rLNKoKN7dmymdNg",
        "edpkvEz2MQvuM2DZhwfUFNAasVgRj6eekRR7tjUqziaA1fXmfsDhTT",
        "edsk42uzLJozidtSbkWvWppoh7pPGhkA7awjJv4e9h257zugpcXwxS" ) );
    ( "user5",
      tez 100,
      ( "dn1NL8VZb7GeWCV8RYA6b3JnEDnh9B7Z2XjT",
        "edpkuuzposdhTc45t3yKDwGzh9o77Q658JD7rWXshaLn7xz1dRa51S",
        "edsk4DRW7NuBW1kCS35QfNzcqRXW7LoNAAQamTFoRHqhzztwkBvg7T" ) );
    ( "user6",
      tez 100,
      ( "dn1Gc5pFRGzBsnTifsNogmhiQcPHvcehbX4s",
        "edpkuYnKcE7XCrG7k3nQ2BYxDxGhwgZ9YhtTg72CKvdk52fqEQLKZJ",
        "edsk4MfoYjGHnSg7paP9dkzNTkdkiFkEHgej93GUKA4GSLgpjeeKJ6" ) );
    ( "user7",
      tez 100,
      ( "dn1SVEoV68ofifURFHeXHVRuDHETmv3HedRa",
        "edpktkozcHKqQ5zmPAMjEfYqDYnmD1NTecMsn9P7hMewd9hi55iKU2",
        "edsk3QvzUrg2ZGks1mwjq8wr7H1f1F9EAbPPafgm4aP5NLStyF2mED" ) );
    ( "user8",
      tez 100,
      ( "dn1d8CXw3GEXKcZvBYzNFWgv4jDEojRXArZo",
        "edpkvLq5HBXAhhEay2sBQesFSCKFa4LN2XFMa6t6GqZwExxRE5qMyA",
        "edsk4YHMekTSDWxECJWQDYpFARsenMefEqfNdXnMfrDJ6brBQrYjGt" ) );
    ( "user9",
      tez 100,
      ( "dn1aMH2u9D53RM74UeosvYh2c3L2k49H7iQa",
        "edpku3H4VrQCy7rZKvAPDR5frz3yh2rzinyCRscWS9m11fVxpiPTnB",
        "edsk2mhS8jTeZDKJTC36HinK2i5DQkfGNeo5bD2vnAQknkm8kiueAt" ) ) ]

*)



let fake swaps =
  Lwt_list.iter_s (fun s ->
      let time = Unix.gettimeofday () in
      let time = CalendarLib.Calendar.from_unixfloat time in
      let ton_amount = Freeton.ton_of_dun s.dun_amount in
      let refund_address = if s.refund_address = "" then
          s.dune_origin else s.refund_address in
      Printf.eprintf "Add swap in DB\n%!";

      let> res = Db.SWAPS.get ~swap_id: s.swap_id in
      match res with
      | None ->
        Db.SWAPS.add { s with dune_status = SwapConfirmed ; time ;
                              ton_amount ; refund_address }
      | Some _ ->
        Printf.eprintf "Swap %d is already in database\n%!" s.swap_id;
        Lwt.return_unit
    ) swaps

type action =
  | No_action
  | Monitor_database
  | Monitor_freeton
  | Close_swap

let () =
  let giver_address = ref None in
  let network_url = ref None in
  let action = ref No_action in
  let dune_swaps = ref [] in
  Arg.parse [
    "--init-db", Arg.Unit (fun () ->
        let dbh = PGOCaml.connect ~database:Project_config.database () in
        EzPG.upgrade_database ~upgrades:Db_common.VERSIONS.(!upgrades) dbh;
        Printf.eprintf "Upgrade done\n%!";
        exit 0
      ),
    " Init database";

    "--giver", Arg.String (fun s -> giver_address := Some s),
    "GIVER Init with Giver address";
    "--network", Arg.String (fun s ->
        network_url := Some (match s with
            | "main" | "mainnet" -> Freeton.mainnet_url
            | "test" | "testnet" -> Freeton.testnet_url
            | _ -> s)),
    "NETWORK_URL Url of freeton node";
    "--save-test-swaps", Arg.Unit (fun () ->
        Array.iteri (fun i s ->
            let s = EzEncoding.construct Encoding_common.swap s in
            EzFile.write_file
              (Printf.sprintf "test-swap-%d.json" (100 + i)) s
          ) test_swaps ;
        Printf.eprintf "Test swaps saved to files.\n%!";
        exit 0
      ),
    " Save test swaps to files";
    "--test-swap", Arg.String (fun s ->
        let s = EzFile.read_file s in
        let s = EzEncoding.destruct Encoding_common.swap s in
        dune_swaps := s :: !dune_swaps
      ),
    "SWAP.json Load a swap from a file (testing)";
    "--db-to-ft", Arg.Unit (fun () -> action := Monitor_database ),
    "Propagate information from Local DB to Free TON";
    "--ft-to-db", Arg.Unit (fun () -> action := Monitor_freeton ),
    "Propagate information from Free TON to Local DB";
    "--close-swap", Arg.Unit (fun () -> action := Close_swap ),
    "Close swap" ;
  ] (fun s -> config_f := s)
    "Dune TON swapper deployer\nUsage:\n\tton-merge-swap-deploy <config.json>";
  try
    Lwt_main.run
      (let> config = get_config ~giver_address:!giver_address
           ~network_url:!network_url in
       let> () = fake !dune_swaps in
       match !action with
       | Monitor_database -> Monitor_database.monitor config
       | Monitor_freeton -> Monitor_freeton.monitor config
       | Close_swap -> Close_swap.close_swap config
       | No_action ->
         Printf.eprintf "Nothing to do. Exiting.\n%!";
         Lwt.return_unit
      )
  with
    Failure s ->
    Format.eprintf "Error: %s@." s;
    exit 2
