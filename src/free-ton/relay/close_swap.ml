open Lwt_utils

(*
open EzCompat
*)
open Ton_sdk
open Data_types

let close_swap config =

  let keypair = match Freeton.keypair with
    | None -> failwith "You must provide a passpharse with TON_MERGE_PASSPHRASE"
    | Some keypair -> keypair
  in
  let client = CLIENT.create config.network_url in
  let server_url = config.network_url in

  let rec close_contract ~abi ~address =
    let> result = REQUEST.post_lwt config.network_url
        ( REQUEST.account ~level:1 address ) in

    match result with
    | Error _ ->
      let> () = Lwt_unix.sleep 3. in
      close_contract ~abi ~address

    | Ok [] -> (* Contract does not exist anymore *)
      Lwt.return_unit
    | Ok ( _ :: _ :: _ ) -> assert false
    | Ok [ acc ] ->
      match acc.acc_type_name, acc.acc_balance with
      | Some "Active", Some balance ->
        if balance > Z.of_int 100_000_000 then
          let> _result =
            Ton_sdk.ACTION.call_lwt ~client ~server_url
              ~address ~abi
              ~meth:"closeSwap" ~params:"{}" ~keypair ~local:false ()
          in
          close_contract ~abi ~address
        else
          (* Balance is too low to be worth killing *)
          Lwt.return_unit

      | _ -> (* Account is not active, we cannot do anything *)
        Lwt.return_unit
  in

  let contracts = ref [] in
  let last_contracts_serial = ref 0l in

  let rec iter () =

    let> addresses = Db.CONTRACTS.list ~serial:!last_contracts_serial in
    if addresses != [] then begin
      List.iter (fun (_address_pubkey, address_address, serial) ->
          if serial > !last_contracts_serial then
            last_contracts_serial := serial;
          contracts := address_address :: !contracts;
        ) addresses ;
      iter ()
    end else
      Lwt.return_unit
  in
  let> () = iter () in


  let rec iter contracts =
    match contracts with
    | [] -> Lwt.return_unit
    | address :: contracts ->
      let> () = close_contract ~abi:Freeton.abi_DuneUserSwap ~address in
      iter contracts
  in
  let> () = iter !contracts in

  let> () = close_contract ~abi:Freeton.abi_DuneEvents
      ~address:config.event_address
  in

  let> () = close_contract ~abi:Freeton.abi_DuneRootSwap
      ~address:config.root_address
  in

  let> () = close_contract ~abi:Freeton.abi_DuneGiver
      ~address:config.giver_address
  in

  Lwt.return_unit
