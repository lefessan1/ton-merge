
open EzCompat
open Ton_sdk
open Data_types

open Lwt_utils

(* TODO: add a table for confirmations *)

let resend_delay = 60. (* instead of 3600. *)

type swap_info = {
  mutable swap : swap ;
  mutable swap_last_query : float ;
}

type address_info = {
  address_pubkey : string ;
  address_address : string ;
  address_last_update : float ;
}

type confirmations = {
  mutable relay_pubkeys : StringSet.t ;
}

let last_confirmation_serial = ref 0l
let last_contracts_serial = ref 0l
let last_logical_time = ref 0L

let swaps_table = Hashtbl.create 711
let addresses_table = Hashtbl.create 711
let confirmations_table = Hashtbl.create 711

let update_logical_time logical_time =
  if logical_time > !last_logical_time then
    last_logical_time := logical_time

(* Load everything that changed in the database since the last
   logical_time, swaps are saved in the swaps_table and contract
   addresses in the addresses_table. *)
let update_swaps () =
  let> logical_time = Db.get_logical_time () in
  let rec iter () =
    if !last_logical_time < logical_time then

      let last_logical_time = !last_logical_time in
      let> swaps = Db.SWAPS.list ~logical_time:last_logical_time () in
      List.iter (fun swap ->
          update_logical_time swap.logical_time;
          match Hashtbl.find swaps_table swap.swap_id with
          | exception Not_found ->
            let ss = {
              swap ;
              swap_last_query = 0. ;
            } in
            Printf.eprintf "New swap: %d\n%!" swap.swap_id ;
            Hashtbl.add swaps_table swap.swap_id ss
          | ss ->
            Printf.eprintf "Updated swap: %d (dune:%s freeton:%s)\n%!"
              swap.swap_id
              ( Encoding_common.string_of_dune_status swap.dune_status )
              ( Encoding_common.string_of_freeton_status swap.freeton_status );
            ss.swap <- swap;
            (* state changed, reset query timer *)
            ss.swap_last_query <- 0.
        ) swaps;

      (* We iterate until last_logical_time = logical_time, which should
         happen when we loaded old the modified records in ascending order. *)
      iter ()

    else
      Lwt.return_unit

  in
  let> () = iter () in



  let rec iter () =

    let> addresses = Db.CONTRACTS.list ~serial:!last_contracts_serial in
    if addresses != [] then
      let address_last_update = Unix.gettimeofday () in
      List.iter (fun (address_pubkey, address_address, serial) ->

          if serial > !last_contracts_serial then
            last_contracts_serial := serial;

          match Hashtbl.find addresses_table address_pubkey with
          | exception Not_found ->
            Printf.eprintf "New contract: %s -> %s\n%!"
              address_pubkey address_address ;
            Hashtbl.add addresses_table address_pubkey
              { address_pubkey ;
                address_address ;
                address_last_update ;
              }

          | old_address ->
            Printf.eprintf "FATAL: address for %s changed %s -> %s\n%!"
              address_pubkey old_address.address_address
              address_address
        ) addresses ;
      iter ()
    else
      Lwt.return_unit

  in
  let> () = iter () in

  let rec iter () =

    let> confs = Db.CONFIRMATIONS.list ~serial:!last_confirmation_serial in

    List.iter (fun (serial, swap_id, relay_pubkey) ->

        if serial > !last_confirmation_serial then
          last_confirmation_serial := serial ;

        Printf.eprintf "New confirm %ld: %d -> %s \n%!"
          serial swap_id relay_pubkey;

        let conf =
          match Hashtbl.find confirmations_table swap_id with
          | exception Not_found ->
            let conf = {
              relay_pubkeys = StringSet.empty ;
            } in
            Hashtbl.add confirmations_table swap_id conf ;
            conf
          | conf -> conf
        in
        conf.relay_pubkeys <- StringSet.add relay_pubkey conf.relay_pubkeys
      ) confs ;

    if confs != [] then
      iter ()
    else
      Lwt.return_unit
  in
  iter ()

let get_swap_address ss =
  match Hashtbl.find addresses_table ss.swap.freeton_pubkey with
  | exception Not_found ->
    Printf.eprintf "Weird: no address, but contract deployed for pubkey %s\n%!"
      ss.swap.freeton_pubkey;
    None
  | address ->
    Some ( address.address_address )

let monitor config =

  let> () = Relay_misc.check_pid "monitor_database" in

  let keypair = match Freeton.keypair with
    | None -> failwith "You must provide a passpharse with TON_MERGE_PASSPHRASE"
    | Some keypair -> keypair
  in
  let client = CLIENT.create config.network_url in
  let server_url = config.network_url in
  let root_address = config.root_address in

  if keypair.public <> config.relay_pubkey then begin
    Printf.eprintf "TON_MERGE_PASSPHRASE differs from pubkey\n%!";
    exit 2
  end ;

  let manage_swap giver_balance ss =
    let curtime = Unix.gettimeofday () in
    let goodtime = curtime -. resend_delay in

    match ss.swap.freeton_status with

    | SwapWaitingForConfirmation ->
      Printf.eprintf "Check confirmed swap: %s\n%!"
        ( Relay_misc.string_of_swap ss.swap );
      begin
        match Hashtbl.find addresses_table ss.swap.freeton_pubkey with
        | exception Not_found ->
          if goodtime < ss.swap_last_query then begin
            Printf.eprintf "-%!" ;
            Lwt.return_unit
          end else begin
            Printf.eprintf "Contract is not yet deployed\n%!";
            let params = Printf.sprintf
                {|{ "pubkey": "0x%s" }|} ss.swap.freeton_pubkey
            in
            ss.swap_last_query <- curtime;
            let> result =
              Ton_sdk.ACTION.call_lwt ~client ~server_url
                ~address:root_address ~abi:Freeton.abi_DuneRootSwap
                ~meth:"deployUserSwap" ~params
                ~keypair ~local:false ()
            in
            begin
              match result with
              | Error exn ->
                Printf.eprintf "Error deploy for pubkey %s: %s\n%!"
                  ss.swap.freeton_pubkey (Printexc.to_string exn) ;
              | Ok res ->
                Printf.eprintf "Deployment asked for pubkey %s: returned %s\n%!"
                  ss.swap.freeton_pubkey res;
            end;
            Lwt.return_unit
          end ;

        | address ->

          if address.address_last_update > ss.swap_last_query then begin
            Printf.eprintf "Contract for %s was just deployed at %s\n%!"
              ss.swap.freeton_pubkey address.address_address;
            ss.swap_last_query <- 0. ;
          end;

          let address = address.address_address in
          if goodtime < ss.swap_last_query then begin
            Printf.eprintf "/%!" ;
            Lwt.return_unit
          end else
            let need_confirmation =
              match Hashtbl.find confirmations_table ss.swap.swap_id with
              | exception Not_found -> true
              | { relay_pubkeys } ->
                not ( StringSet.mem config.relay_pubkey relay_pubkeys )
            in
            if need_confirmation then
              let params =
                let `Hex hashed_secret = Hex.of_bytes ss.swap.hashed_secret in
                Printf.sprintf
                  {|
{
  "order_id": "%s",
  "hashed_secret": "0x%s",
  "ton_amount": "%s",
  "dun_amount": "%s",
  "dest": "%s",
  "depool": "%s"
}
|}
                  (Freeton.json_of_swap_id ss.swap.swap_id) (* string *)
                  hashed_secret  (* uint256 *)
                  (Z.to_string ss.swap.ton_amount) (* uint64 *)
                  (Z.to_string ss.swap.dun_amount) (* uint64 *)
                  ss.swap.freeton_address (* address *)
                  (match ss.swap.freeton_depool with
                   | Some addr -> addr
                   | None -> Freeton.addr_zero
                  ) (* address *)
              in
              ss.swap_last_query <- curtime;
              let> result =
                Ton_sdk.ACTION.call_lwt ~client ~server_url
                  ~address ~abi:Freeton.abi_DuneUserSwap
                  ~meth:"confirmOrder" ~params
                  ~keypair ~local:false ()
              in
              begin
                match result with
                | Error exn ->
                  Printf.eprintf "Error confirmOrder for pubkey %s: %s\nparams: %s\n%!"
                    ss.swap.freeton_pubkey (Printexc.to_string exn) params;
                | Ok res ->
                  Printf.eprintf "confirmOrder asked for swap %d: returned %s\n%!"
                    ss.swap.swap_id res;
              end;
              Lwt.return_unit
            else
              Lwt.return_unit
      end

    | SwapFullyConfirmed ->
      (* wait for credit. TODO: If too long, use requestCredit() ? *)
      Lwt.return_unit

    | SwapWaitingForCredit ->
      (* wait for credit *)
      Lwt.return_unit

    | SwapCreditDenied ->

      if !giver_balance > ss.swap.ton_amount &&
         goodtime > ss.swap_last_query then begin

        match get_swap_address ss with
        | None -> Lwt.return_unit
        | Some address ->
          let params = Printf.sprintf {|{ "order_id": "%s" }|}
              (Freeton.json_of_swap_id ss.swap.swap_id)
          in
          ss.swap_last_query <- curtime;
          let> result =
            Ton_sdk.ACTION.call_lwt ~client ~server_url
              ~address ~abi:Freeton.abi_DuneUserSwap
              ~meth:"requestCredit" ~params
              ~keypair ~local:false ()
          in
          (* We don't really care about the result, we will learn later if
             it was successful. *)
          begin
            match result with
            | Ok _ ->
              giver_balance := Z.sub !giver_balance ss.swap.ton_amount
            | _ -> ()
          end;
          Lwt.return_unit
      end else
        Lwt.return_unit

    | SwapCredited ->
      (* TODO: if we know the secret, we should reveal it *)
      begin
        match ss.swap.secret with
        | None ->
          Lwt.return_unit
        | Some secret ->
          if !giver_balance > ss.swap.ton_amount &&
             goodtime > ss.swap_last_query then begin

            match get_swap_address ss with
            | None -> Lwt.return_unit
            | Some address ->
              let params = Printf.sprintf
                  {|{ "order_id": "%s", "secret": "%s" }|}
                  (Freeton.json_of_swap_id ss.swap.swap_id)
                  (Freeton.json_of_string secret)
              in
              ss.swap_last_query <- curtime;
              let> _result =
                Ton_sdk.ACTION.call_lwt ~client ~server_url
                  ~address ~abi:Freeton.abi_DuneUserSwap
                  ~meth:"revealOrderSecret" ~params
                  ~keypair ~local:false ()
              in
              Lwt.return_unit

          end
          else
            Lwt.return_unit
      end

    | SwapRevealed ->
      Lwt.return_unit

    | SwapWaitingForDepool ->
      Lwt.return_unit

    | SwapDepoolDenied ->
      (* TODO: we should warn the user, he should declare the donor or change
         the depool *)
      Lwt.return_unit

    | SwapTransferred -> (* we are done, wait for Dune side *)
      begin
        match ss.swap.dune_status with
        | SwapCompleted ->
          Hashtbl.remove swaps_table ss.swap.swap_id;
          Lwt.return_unit
        | _ ->
          Lwt.return_unit
      end

    | SwapCancelled -> (* we are done, wait for Dune side *)
      begin
        match ss.swap.dune_status with
        | SwapRefunded ->
          Hashtbl.remove swaps_table ss.swap.swap_id;
          Lwt.return_unit
        | _ ->
          Lwt.return_unit
      end
  in

  let rec iter () =
    let> () = Lwt_unix.sleep 1.0 in
    Printf.eprintf ".%!";

    let> () = update_swaps () in

    let swaps = ref [] in
    Hashtbl.iter (fun _ ss ->
        swaps := ss :: !swaps
      ) swaps_table ;

    let> giver_balance =
      let> result = REQUEST.post_lwt config.network_url
          (REQUEST.account ~level:1 config.giver_address) in
      match result with
      | Ok [ acc ] -> begin
          match acc.acc_balance with
          | None -> assert false
          | Some z -> Lwt.return z
        end
      | _ -> assert false

    in
    let giver_balance = ref giver_balance in

    let> () = Lwt_list.iter_s (manage_swap giver_balance) !swaps in
    iter ()
  in
  iter ()
