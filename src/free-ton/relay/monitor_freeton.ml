
open EzCompat
open Ton_sdk
open Data_types
open Lwt_utils

let wait_before_retry delay =
  Lwt_unix.sleep (float_of_int delay)

let map_of_json ?(root=[]) json =
  let json = Ezjsonm.from_string json in
  let map = ref StringMap.empty in
  let add path s =
    let key = String.concat ":" ( List.rev path ) in
    map := StringMap.add key s !map
  in
  let rec iter path json =
    add path json;
    match json with
      `O list ->
        List.iter (fun (s,v) ->
            iter (s :: path) v
          ) list
    | `A list ->
        List.iteri (fun i v ->
            iter (string_of_int i :: path) v
          ) list
    | `Bool _
    | `Null
    | `Float _
    | `String _ -> ()
  in
  iter (List.rev root) json;
  !map

let fatal loc =
  Printf.eprintf "Fatal error at %s\n%!" loc;
  exit 2

let iter_event ~network_url ~abi ~client ~msg_id f =
  let rec iter () =
    let> result = REQUEST.post_lwt network_url
        (REQUEST.messages ~level:3 ~id:msg_id []) in
    match result with
    | Error exn ->
      Printf.eprintf "iter_message: exception %s\n%!"
        (Printexc.to_string exn);
      let> () = wait_before_retry 3 in
      iter ()
    | Ok [ msg ] ->
      Printf.printf "  MESSAGE: %s\n%!"
        ( ENCODING.string_of_message msg ) ;
      begin (* decode_message only works when there is a msg_body too *)
        match msg.msg_body with
        | None -> Lwt.return_unit
        | Some _body ->
          match msg.msg_boc with
          | None -> fatal __LOC__
          | Some boc ->
            match BLOCK.decode_message_boc ~client ~boc ~abi with
            | decoded ->
              if decoded.body_type = 3 (* Event *) then
                f decoded.body_name decoded.body_args
              else
                Lwt.return_unit
            | exception _exn ->
              fatal __LOC__
      end
    | _ -> fatal __LOC__
  in
  iter ()

let rec iter_transactions ~block_id ~network_url ~address f =
  let> res =
    REQUEST.post_lwt network_url
      (REQUEST.transactions
         ~level:3
         ~block_id
         ~account_addr:address [])
  in
  match res with
  | Ok [] -> Lwt.return_unit
  | Ok trs ->
    Printf.eprintf "In block with id: %S\n%!" block_id;
    Lwt_list.iter_s (fun tr ->
        Printf.eprintf "\nTRANSACTION: %s\n%!"
          (ENCODING.string_of_transaction tr);
        f tr) trs
  | Error _ ->
    let> () = wait_before_retry 3 in
    iter_transactions ~block_id ~network_url ~address f

let timeout = 2_000_000_000L

let iter_blocks ~client ~block_id ~address f =
  let rec iter block_id =
    match BLOCK.wait_next_block
            ~client ~block_id ~address
            ~timeout () with
    | b ->
      let block_id = b.id in
      Printf.eprintf "new blockid: %S\n%!" b.id;
      Printf.eprintf "block = %s\n%!"
        (Ton_sdk.TYPES.string_of_block b) ;
      let> () = f b in
      iter block_id
    | exception exn ->
      Printf.eprintf "Exception wait_next_block: %s\n%!"
        ( Printexc.to_string exn );
      let> () = wait_before_retry 3 in
      iter block_id
  in
  iter block_id


let monitor config =

  let> () = Relay_misc.check_pid "monitor_freeton" in

  let network_url = config.network_url in
  let client = Ton_sdk.CLIENT.create network_url in
  let address = config.event_address in
  let abi = Freeton.abi_DuneEvents in

  let handle_event event_name event_args =
    let event_args = match event_args with
      | None -> "{}"
      | Some args -> args in
    Printf.eprintf "*\n**\n***\n\n\n";
    Printf.eprintf "EVENT: %s %s\n\n\n\n%!" event_name event_args;
    let args = map_of_json event_args in
    match event_name with

    | "UserSwapDeployed" ->
      let pubkey = StringMap.find "pubkey" args in
      let user_addr = StringMap.find "user_addr" args in
      begin
        match pubkey, user_addr with
        | `String pubkey, `String user_addr ->
          let pubkey = Freeton.pubkey_of_json pubkey in
          Printf.eprintf "EVENT UserSwapDeployed(%s, %s)\n%!"
            pubkey user_addr ;

          Db.CONTRACTS.set
            ~name:pubkey ~address:user_addr

        | _ -> fatal __LOC__
      end

    | "OrderStateChanged" ->
      let order_id = StringMap.find "order_id" args in
      let state_count = StringMap.find "state_count" args in
      let state = StringMap.find "state" args in
      begin
        match order_id, state_count, state with
        | `String order_id,
          `String state_count,
          `String state ->
          let swap_id =
            Freeton.swap_id_of_json order_id in
          let state_count = Int32.of_string state_count in
          Printf.eprintf
            "EVENT OrderStateChanged(%d, %ld, %s)\n%!"
            swap_id state_count state ;

          let> res =
            Db.SWAPS.get_freeton_status ~swap_id in

          let old_state_count = match res with
              Some ( old_state_count, _old_status ) -> old_state_count
            | None -> 0l
          in

          if old_state_count < state_count then
            Db.SWAPS.set_freeton_status ~swap_id
              state_count
              ( match state with
                | "0" -> SwapWaitingForConfirmation
                | "1" -> SwapFullyConfirmed
                | "2" -> SwapWaitingForCredit
                | "3" -> SwapCreditDenied
                | "4" -> SwapCredited
                | "5" -> SwapRevealed
                | "6" -> SwapWaitingForDepool
                | "7" -> SwapDepoolDenied
                | "8" -> SwapTransferred
                | "9" -> SwapCancelled
                | _ ->
                  Printf.eprintf "Unexpected freeton status %S\n%!" state;
                  fatal __LOC__
              )
          else
            Lwt.return_unit

        | _ -> fatal __LOC__
      end

    | "OrderCredited" -> (* sent by Giver *)
      let order_id = StringMap.find "order_id" args in
      let accepted = StringMap.find "accepted" args in
      begin
        match order_id, accepted with
          `String order_id, `Bool accepted ->
          let swap_id =
            Freeton.swap_id_of_json order_id in
          Printf.eprintf
            "EVENT OrderCredited(%d, %b)\n%!"
            swap_id accepted ;
          Lwt.return_unit
        | _ -> fatal __LOC__
      end

    | "OrderSecretRevealed" ->
      let order_id = StringMap.find "order_id" args in
      let secret = StringMap.find "secret" args in
      begin
        match order_id, secret with
          `String order_id, `String secret ->
          let swap_id =
            Freeton.swap_id_of_json order_id in
          let secret = Freeton.string_of_json secret in
          Printf.eprintf
            "EVENT OrderSecretRevealed(%d, %S)\n%!"
            swap_id secret ;

          Db.SWAPS.set_secret ~swap_id (Bytes.of_string secret)

        | _ -> fatal __LOC__
      end

    | "OrderConfirmedByRelay" ->
      let order_id = StringMap.find "order_id" args in
      let relay_pubkey = StringMap.find "pubkey" args in
      begin
        match order_id, relay_pubkey with
          `String order_id, `String relay_pubkey ->
          let swap_id =
            Freeton.swap_id_of_json order_id in
          let relay_pubkey =
            Freeton.pubkey_of_json relay_pubkey in
          Printf.eprintf
            "EVENT OrderConfirmedByRelay(%d, %s)\n%!"
            swap_id relay_pubkey ;

          Db.CONFIRMATIONS.add ~swap_id relay_pubkey
        | _ -> fatal __LOC__
      end


    | _ ->
      Lwt.return_unit
  in
  iter_blocks
    ~block_id:config.last_blockid
    ~client
    ~address
    (fun b ->
       let> () =
         iter_transactions
           ~block_id:b.id
           ~network_url
           ~address
           (fun tr ->
              Lwt_list.iter_s (fun msg_id ->
                  iter_event ~abi ~network_url ~client ~msg_id handle_event
                )
                tr.tr_out_msgs
           )
       in
       Db.CONFIG.set_last_blockid b.id
    )
