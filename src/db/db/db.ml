open Lwt_utils
open Db_common
open Data_types
open Encoding_common

(* utils *)

let with_dbh f x =
  let>>> dbh = () in
  f dbh x

let catch_pg_unique msg f =
  Lwt.catch f @@ fun e ->
  match pg_error_of_exn e with
  | UniqueViolation u_msg ->
    Format.ksprintf Lwt.return_error "%s (%s)" msg u_msg
  | OtherPGError _ | OtherError _ -> raise e

(* let cal_to_str ?(ms=true) date =
 *   let s = CalendarLib.Printer.Calendar.sprint "%Y-%m-%dT%H:%M:%S" date in
 *   if ms then s ^ ".000Z" else s ^ "Z"
 *
 * let cal_of_str ?(ms=true) s =
 *   try
 *     float_of_string s
 *     |> CalendarLib.Calendar.from_unixfloat
 *   with _ ->
 *     let s =
 *       if ms then String.sub s 0 (String.length s - 5)
 *       else String.sub s 0 (String.length s - 1) in
 *     CalendarLib.Printer.Calendar.from_fstring "%Y-%m-%dT%H:%M:%S" s *)

let string_of_op_status = function
  | OpQueued -> "queued"
  | OpPending -> "pending"
  | OpUnconfirmed -> "unconfirmed"
  | OpConfirmed -> "confirmed"
  | OpError -> "error"

let op_status_of_string = function
  | "queued" -> OpQueued
  | "pending" -> OpPending
  | "unconfirmed" -> OpUnconfirmed
  | "confirmed" -> OpConfirmed
  | "error" -> OpError
  | s -> invalid_arg ("op_status_of_string: " ^ s)

let db_block r = {
  block_hash = r#block_hash;
  block_predecessor = r#block_predecessor;
  block_level = r#block_level;
  block_timestamp = r#block_timestamp;
}

let db_opt_block r =
  match r#block_hash, r#block_level, r#block_timestamp with
  | Some block_hash, Some block_level, Some block_timestamp ->
    Some {
      block_hash;
      block_predecessor = r#block_predecessor;
      block_level;
      block_timestamp;
    }
  | _ -> None

let db_block_opt r = {
  block_hash = Option.get r#block_hash;
  block_predecessor = r#block_predecessor;
  block_level = Option.get r#block_level;
  block_timestamp = Option.get r#block_timestamp;
}

(* base *)

let db_version = function [ Some v ] -> Int32.to_int v | _ -> 0

let get_version () =
  let>>> dbh = () in
  let>! r = [%pgsql dbh "select value from ezpg_info where name = 'version'"] in
  db_version r

let get_smart_contract_address name =
  let>>> dbh = () in
  [%pgsql dbh "select address from smart_contracts where name = $name"]
  >|= function
  | [] -> None
  | [addr] -> Some addr
  | _ -> assert false

let register_smart_contract_address ~name ~addr =
  let>>> dbh = () in
  [%pgsql dbh "insert into smart_contracts (name, address)  \
               values ($name, $addr) \
               on conflict (name) do \
               update set \
               name = $name,
              address = $addr"]

(* Config *)

let set_config_option key value encoding =
  let value = Json_encoding.construct encoding value in
  let>>> dbh = () in
  [%pgsql dbh
      "insert into blockchain_config (key, value_json) values ($key, $value) \
       on conflict (key) do update set value_json = $value"
  ]

let get_config_option key encoding =
  let>>> dbh = () in
  [%pgsql dbh
      "select value_json  from blockchain_config where key = $key"
  ] >|= function
  | [] -> None
  | v :: _ -> Some (Json_encoding.destruct encoding v)



(* Crawler *)

let block_registered hash =
  let>>> dbh = () in
  let>+ r = [%pgsql dbh "select main from blocks where hash = $hash"] in
  match r with
  | [ main ] -> true, main
  | _ -> false, false

let last_level () =
  let>>> dbh = () in
  let>! r = [%pgsql dbh "select max(level) from blocks where main"] in
  match r with
  | [ Some level ] -> Int32.to_int level
  | _ -> 0

let register_block_operation dbh ~block_hash
    ~operation_hash ~index ~success (op : Dune.Types.node_operation_type) =
  let op_json =
    Json_encoding.construct
      Dune.Encoding.Operation.contents_and_result_encoding op
    |> Ezjsonm.value_to_string in
  let>! r = [%pgsql dbh
      "insert into block_operations(block, operation, op_index, success, op_content) \
       values($block_hash, $operation_hash, $index, $success, $op_json) \
       on conflict on constraint block_operations_pkey do nothing \
       returning operation, op_index"] in
  match r with
  | [ _ ] -> `New
  | _ -> `AlreadyKnown

let register_block_if_unknown dbh (b : block) =
  (* let block_tsp = cal_of_str ~ms:false b.block_timestamp in *)
  let>! r = [%pgsql dbh "nullable-results"
      "with ins as (\
         insert into blocks(hash, level, predecessor, block_timestamp, main) \
         values(${b.block_hash}, ${b.block_level}, \
         $?{b.block_predecessor}, ${b.block_timestamp}, false) \
         on conflict(hash) do nothing \
         returning hash, main)\
       select ins.main, blocks.main from ins \
       full join blocks on ins.hash = blocks.hash \
       where ins.hash = ${b.block_hash} or blocks.hash = ${b.block_hash}
      "] in
  match r with
  | [Some _, _] -> `New
  | [None, Some main] -> `AlreadyKnown main
  | _ -> assert false

let db_operation_status db_block r =
  match Option.get r#status with
  | OpQueued -> Queued
  | OpPending ->
    Pending {
      operation_hash = Option.get r#op_hash;
      index = Option.value ~default:0 r#op_index;
      internal_index = Option.value ~default:0 r#internal_index;
    }
  | OpUnconfirmed ->
    Unconfirmed {
      operation_hash = Option.get r#op_hash;
      index = Option.value ~default:0 r#op_index;
      internal_index = Option.value ~default:0 r#internal_index;
      block = db_block r;
      confirmations = Option.value ~default:0l r#confirmations |> Int32.to_int;
    }
  | OpConfirmed ->
    Confirmed {
      operation_hash = Option.get r#op_hash;
      index = Option.value ~default:0 r#op_index;
      internal_index = Option.value ~default:0 r#internal_index;
      block = db_block r;
    }
  | OpError ->
    let msg = Option.value ~default:"" r#op_error in
    let err_kind = match r#retry with
      | None -> `Permanent
      | Some i -> `Retry i in
    Errored { err_kind; msg }

let status_to_params = function
  | Queued -> OpQueued, None, None, None, None, None
  | Pending {
      operation_hash;
      index;
      internal_index;
    } -> OpPending, None, Some operation_hash, Some index, Some internal_index, None
  | Unconfirmed {
      operation_hash;
      index;
      internal_index;
      block;
      confirmations;
    } ->
    OpUnconfirmed, Some block, Some operation_hash, Some index, Some internal_index,
    Some (Int32.of_int confirmations)
  | Confirmed  {
      operation_hash;
      index;
      internal_index;
      block;
    } ->
    OpUnconfirmed, Some block, Some operation_hash, Some index, Some internal_index,
    None
  | Errored _ -> OpError, None, None, None, None, None


let db_block_hash r = r#block_hash
let db_block_hash_opt r = Option.get r#block_hash

let db_get_operation db_get_block op =
  {
    op_id = Option.get op#id;
    op_source = Option.get op#source;
    op_status = db_operation_status db_get_block op;
    op_content =
      Json_encoding.destruct operation_content
        (Option.get op#op_content);
  }


let list_operations ?confirmed ?status ?origin ?kind () =
  let>>> dbh = () in
  let>! r = [%pgsql.object dbh "nullable-results" (* because of left join *)
      "select ops.id, source, op_content, status, op_hash, op_error, \
       confirmations, op_index, internal_index, retry, \
       block_hash, b.level as block_level, b.predecessor as block_predecessor, \
       b.block_timestamp \
       from operations as ops \
       left join blocks b on block_hash = b.hash \
       where \
       ($?{confirmed}::boolean is null or \
       ($?confirmed = true and \
        not (status = 'queued' or status = 'pending' or status = 'unconfirmed')) or \
       ($?confirmed = false and \
        (status = 'queued' or status = 'pending' or status = 'unconfirmed'))) \
       and ($?kind::text is null or op_content->>'kind' = $?kind)
       and status = coalesce($?status, status) \
       and ($?origin::text is null or source = $?origin)"
  ]
  (* TODO:
     op_content->>'accounts' @@ '$.account_id = account_id_str'
     if/when using postgres V12 for mint operations
  *)
  in
  List.filter_map (fun r ->
      try
        let op = db_get_operation db_opt_block r in
        Some op
      with e ->
        let sid = match r#id with
          | None -> "?"
          | Some id -> Int32.to_string id in
        Format.eprintf "[@<hov 4> Warning: could not decode operation %s :@ %a@]@." sid
          (Json_encoding.print_error ?print_unknown:None) e;
        None
    ) r


let update_confirmations_operations ~confirmations_needed dbh level =
  let confirmed_level = Int32.sub level confirmations_needed  in
  let>! l =
    [%pgsql.object dbh "nullable-results"
      "with updated as ( \
         update operations set \
         confirmations = $level - level, \
         status = case \
                    when level <= $confirmed_level \
                    then 'confirmed' else status \
                  end \
         from blocks as b \
         where status = 'unconfirmed' and block_hash = b.hash and b.main
         returning *
       ) \
       select u.id, source, u.op_content, status, op_hash, op_error, retry, \
       confirmations, u.op_index, u.internal_index, \
       block_hash, level as block_level, predecessor as block_predecessor, \
       block_timestamp, bo.success, bo.op_content as dune_op \
       from updated u \
       left join block_operations bo on bo.op_index = u.op_index \
       where \
       bo.operation = op_hash"
    ] in
  List.map (fun r ->
      let success = Option.value ~default:false r#success in
      let op =
        r#dune_op
        |> Option.map (fun p ->
            p
            |> Ezjsonm.value_from_string
            |> Json_encoding.destruct Dune.Encoding.Operation.contents_and_result_encoding
          )
      in
      let dop = db_get_operation db_block_hash r in
      dop, success, op) l

let mark_block_main dbh ~main ~block_hash =
  [%pgsql dbh
      "update blocks set main = $main where hash = $block_hash and main <> $main \
       returning level"
  ]

let mark_operations_in_main dbh ~block_hash =
  [%pgsql dbh
      "update operations o set \
       block_hash = bo.block, \
       status = 'unconfirmed', \
       confirmations = 0 \
       from block_operations as bo \
       where bo.operation = op_hash \
       and bo.block = $block_hash \
       and bo.op_index = o.op_index"
  ]

let mark_operations_in_alt dbh ~block_hash =
  [%pgsql dbh
      "update operations o set \
       block_hash = NULL, \
       status = 'error', \
       op_error = 'A fork occured in the chain, the operation was included in a block on an alternative branch', \
       confirmations = NULL \
       from block_operations as bo \
       where bo.operation = op_hash \
       and bo.block = $block_hash \
       and bo.op_index = o.op_index"
  ]

let get_block_operations ~confirmations_needed dbh ~main ~block_hash ~level =
  let confirmed_level = Int32.sub level confirmations_needed  in
  [%pgsql.object dbh
      "select (level = $confirmed_level) as confirmed, success, \
       block, operation, op_index, op_content as dune_op \
       from block_operations \
       join blocks on hash = block \
       where main = $main and \
       (block = $block_hash or level = $confirmed_level)"
  ] >|=
  List.map (fun r ->
      let confirmed = Option.value ~default:false r#confirmed in
      let op =
        r#dune_op
        |> Ezjsonm.value_from_string
        |> Json_encoding.destruct Dune.Encoding.Operation.contents_and_result_encoding
      in
      confirmed, r#success, r#block, r#operation, r#op_index, op
    )

let block_predecessor dbh hash =
  let>+ r = [%pgsql dbh "select predecessor, main from blocks where hash = $hash"] in
  r

let head_hash dbh =
  let> r = [%pgsql dbh "select hash from blocks where main order by level desc limit 1"] in
  match r with
  | [ hash ] -> Lwt.return_some hash
  | _ -> Lwt.return_none


(* App data *)

(* - Operations *)

let mark_operation_error
    ~retry_nb operation_id err_msg err_kind =
  let retry_nb = Int32.of_int retry_nb in
  let retry = match err_kind with
    | `Permanent -> false
    | `Temporary -> true in
  let>>> dbh = () in
  [%pgsql dbh
      "update operations set \
       status = 'error', \
       retry = case \
         when not $retry \
         then null \
         when retry is null \
         then $retry_nb \
         when retry <= 1 \
         then null \
         else retry - 1 \
       end, \
       op_error = case when op_error is null \
                  then $err_msg \
                  else op_error || '\n' || $err_msg end \
       where id = $operation_id
       returning id"]
  >>= function
  | [] -> Lwt.return_error "Cannot mark operation error"
  | _ -> Lwt.return_ok ()

let db_update_pending_operation dbh operation_id ~op_hash ~op_index =
  [%pgsql dbh
      "update operations set \
       status = 'pending', \
       op_hash = $op_hash, \
       op_index = $op_index \
       where id = $operation_id"
  ]

let get_logical_time dbh () =
  let> res = [%pgsql dbh
      "select value from logical_time where name = 'time'"]
  in
  match res with
  | [ value ] -> Lwt.return value
  | [] ->
    let> () = [%pgsql dbh
        "insert into logical_time(name,value) values ('time',1)"
    ]
    in
    Lwt.return 1L
  | _ -> assert false

let set_logical_time dbh (time : int64) =
  [%pgsql dbh
      "update logical_time set value = $time where name = 'time'"
  ]

(* Everytime we update swaps or addresses, we increase a counter, the
   logical_time. Thanks to this, we can easily query changes that are
   more recent than a given logical time. *)


let warn_retrying exn =
      Printf.eprintf "Tranaction aborted (%s). Retrying\n%!"
        (Printexc.to_string exn)

module LOGICAL_TIME : sig
  val with_increased_logical_time :
    (database_handle -> int64 -> 'a Lwt.t) -> ('a, exn) result Lwt.t
  val with_increased_logical_time_iter :
    (database_handle -> int64 -> 'a Lwt.t) -> 'a Lwt.t
  val get_logical_time : unit -> int64 Lwt.t
end = struct
  let with_increased_logical_time f =
    Db_common.try_transaction (fun dbh ->
        let> time = get_logical_time dbh () in
        let time = Int64.add time 1L in
        let> r = f dbh time in
        let> () = set_logical_time dbh time in
        Lwt.return r
      )

  let with_increased_logical_time_iter f =
    let rec iter n =
      let> res = with_increased_logical_time f in
      match res with
      | Ok v -> Lwt.return v
      | Error exn ->
        warn_retrying exn;
        if n = 100 then begin
          Printf.eprintf "Aborting after 100 retries.\n%!";
          exit 2
        end ;
        iter (n+1)
    in
    iter 0

  let get_logical_time () = with_dbh get_logical_time ()

end
include LOGICAL_TIME

let update_pending_operation operation_id ~op_hash ~op_index =
  let>>> dbh = () in
  db_update_pending_operation dbh operation_id ~op_hash ~op_index

let update_pending_batch ~op_hash batch =
  let>>> dbh = () in
  Lwt_list.iter_s (fun (op_index, op) ->
      db_update_pending_operation dbh op.op_id ~op_hash ~op_index
    ) batch

let add_operation dbh ~source ~status op =
  (* let>>> dbh = () in *)
  let status, block_hash, op_hash, op_index, internal_index, confirmations =
    status_to_params status in
  let content = Json_encoding.construct operation_content op in
  (* [%pgsql dbh
   *     "insert into operations(source, op_content, status, block_hash, op_hash, op_index, internal_index, confirmations) \
   *      values($source, $content, $status, $?block_hash, $?op_hash, $?op_index, $?internal_index, $?confirmations) \
   *      on conflict on constraint opref_unique do update set \
   *      status = $status, \
   *      block_hash = $?block_hash, \
   *      op_hash = $?op_hash, \
   *      op_index = $?op_index, \
   *      internal_index = $?internal_index, \
   *      confirmations = $?confirmations \
   *      returning id \
   *      "] *)
  [%pgsql dbh
      "update operations set \
       status = $status, \
       block_hash = $?block_hash, \
       op_hash = $?op_hash, \
       op_index = $?op_index, \
       internal_index = $?internal_index, \
       confirmations = $?confirmations \
       where \
       (block_hash is null or block_hash = $?block_hash) and \
       (op_hash is null or op_hash = $?op_hash) and \
       (op_index is null or op_index = $?op_index) and \
       (internal_index is null or internal_index = internal_index) \
       returning id \
       "]
  >>= function
  | [x] -> Lwt.return x
  | _ :: _ :: _ -> assert false
  | [] ->
    [%pgsql dbh
        "insert into operations(source, op_content, status, block_hash, op_hash, op_index, internal_index, confirmations) \
         values($source, $content, $status, $?block_hash, $?op_hash, $?op_index, $?internal_index, $?confirmations) \
         returning id"]
    >|= function
    | [x] -> x
    | [] -> failwith "No operation queued"
    | _ -> assert false

let set_swap_id_for_deposit dbh ~op_id ~swap_id =
  [%pgsql dbh
      "update operations set \
       op_content = jsonb_set(op_content, '{swap_id}', ${string_of_int swap_id}) \
       where id = $op_id"]

(* Swapper *)

let set_swapper_info s =
  let json = Json_encoding.construct swapper_info s in
  let>>> dbh = () in
  [%pgsql dbh
        "insert into swapper_info (value_json) values($json) \
         on conflict (id) do update set value_json = $json" ]

let get_swapper_info () =
  let>>> dbh = () in
  [%pgsql dbh "select value_json from swapper_info" ]
  >|= function
  | [v] -> Some (Json_encoding.destruct swapper_info v)
  | _ -> None




let set_swaps_big_map id =
  let>>> dbh = () in
  [%pgsql dbh
      "insert into big_map_ids (name, big_map_id) values('swaps', ${Int32.of_int id}) \
       on conflict (name) do update set big_map_id = ${Int32.of_int id}" ]

let get_swaps_big_map () =
  let>>> dbh = () in
  [%pgsql dbh "select big_map_id from big_map_ids where name = 'swaps'" ]
  >|= function
  | [id] -> Some (Int32.to_int id)
  | _ -> None

let address_known_in_db addr =
  let>>> dbh = () in
  [%pgsql dbh "select name from smart_contracts where address = $addr"]
  >|= function
  | [] -> false
  | _ -> true

let register_swap_contract_address ~addr =
  register_smart_contract_address ~name:"swap" ~addr

let get_swap_contract_address () =
  get_smart_contract_address "swap"

(*
let mark_swap_allocated swap_id =
  let>>> dbh = () in
  [%pgsql dbh
      "update swaps set freeton_allocated = true \
       where id = ${Int32.of_int swap_id}"]
*)

(*
let get_swap_allocated swap_id =
  let>>> dbh = () in
  [%pgsql dbh
      "select freeton_allocated from swaps \
       where id = ${Int32.of_int swap_id}"]
  >|= function
  | [true] -> true
  | _ -> false
*)


(* Swaps *)

module SWAPS = struct

  (* This function assumes the swap does not exist. You should make sure
     it it the case before calling it. *)
  let add
      {swap_id; dune_origin; dun_amount; ton_amount;
       dune_status; freeton_status;
       hashed_secret; time; refund_address;
       freeton_pubkey; freeton_address; freeton_depool;
       secret ; confirmations ;
       logical_time = _ ;
      } =
    with_increased_logical_time_iter (fun dbh logical_time ->
        [%pgsql dbh
            "insert into swaps ( \
             id, dune_origin, dun_amount, ton_amount, \
             dune_status, freeton_status, freeton_state_count, \
             hashed_secret, swap_timestamp, refund_address, \
             freeton_pubkey, freeton_address, freeton_depool, \
             confirmations, secret, logical_time) \
             values ( \
             ${Int64.of_int swap_id}, $dune_origin, $dun_amount, $ton_amount, \
             $dune_status, $freeton_status, 0, \
             $hashed_secret, $time, $refund_address, \
             $freeton_pubkey, $freeton_address, $?freeton_depool, \
             ${Int32.of_int confirmations}, \
             $?{Option.map Bytes.of_string secret}, \
             ${logical_time} \
             )"]
      )

  let get_dune_status ~swap_id =
    let>>> dbh = () in
    let id = Int64.of_int swap_id in
    [%pgsql.object dbh "select dune_status from swaps where id = $id" ]
    >|= function
    | [s] -> Some s#dune_status
    | _ -> None

  let set_dune_status ~swap_id new_status =
    let> old_status = get_dune_status ~swap_id in
    match old_status with
    | None ->
      Printf.eprintf "Fatal error: get_swap_status(%d) failed\n%!" swap_id;
      assert false
    | Some old_status ->
      if old_status <> new_status then
        match old_status, new_status with
        | SwapRefundAsked, SwapRefunded
        | SwapSubmitted, SwapConfirmed
          ->
          with_increased_logical_time_iter (fun dbh logical_time ->
              [%pgsql dbh
                  "update swaps set dune_status = $new_status, \
                   logical_time=$logical_time where id = \
                   ${Int64.of_int swap_id}" ]
            )
        | _ ->
          Printf.eprintf "Fatal error: set_swap_status from %s to %s\n%!"
            ( Encoding_common.string_of_dune_status old_status )
            ( Encoding_common.string_of_dune_status new_status );
          assert false
      else
        Lwt.return_unit

  let get_freeton_status ~swap_id =
    let>>> dbh = () in
    let id = Int64.of_int swap_id in
    [%pgsql dbh "select freeton_state_count, freeton_status \
                        from swaps where id = $id" ]
    >|= function
    | [s] -> Some s
    | _ -> None

  let set_freeton_status ~swap_id state_count new_status =
    with_increased_logical_time_iter (fun dbh logical_time ->
        [%pgsql dbh
            "update swaps set \
             freeton_status = $new_status, \
             freeton_state_count = $state_count, \
             logical_time=$logical_time where id = \
             ${Int64.of_int swap_id}" ]
      )

  let db_swap s = {
    swap_id = Int64.to_int s#id;
    dune_origin = s#dune_origin;
    dun_amount = s#dun_amount;
    ton_amount = s#ton_amount;
    dune_status = s#dune_status;
    freeton_status = s#freeton_status;
    hashed_secret = s#hashed_secret;
    time = s#swap_timestamp;
    refund_address = s#refund_address;
    freeton_pubkey = s#freeton_pubkey;
    freeton_address = s#freeton_address;
    freeton_depool = s#freeton_depool;
    secret = Option.map Bytes.to_string s#secret;
    confirmations = Int32.to_int s#confirmations;
    logical_time = s#logical_time;
  }

  let get ~swap_id =
    let>>> dbh = () in
    let id = Int64.of_int swap_id in
    [%pgsql.object dbh "select * from swaps where id = $id" ]
    >|= function
    | [s] -> Some (db_swap s)
    | _ -> None

  (* Do not return more than 100 swaps, oldest first *)
  let list ?logical_time () =
    let>>> dbh = () in
    let>! l =
      match logical_time with
      | None ->
        [%pgsql.object dbh "select * from swaps \
            order by logical_time asc limit 100" ]
      | Some logical_time ->
        [%pgsql.object dbh
            "select * from swaps where logical_time > $logical_time \
            order by logical_time asc limit 100
            " ]
    in
    List.rev_map db_swap l |> List.rev

  let count () =
    let>>> dbh = () in
    [%pgsql dbh "select count(*) from swaps" ]
    >|= function
    | [Some c] -> Int64.to_int c
    | _ -> assert false

  let set_secret ~swap_id secret =
    with_increased_logical_time_iter (fun dbh logical_time ->
        [%pgsql dbh
            "update swaps set secret = $secret, \
             logical_time = $logical_time \
             where id = ${Int64.of_int swap_id}"]
      )

  let get_swaps_by_origin origin =
    let>>> dbh = () in
    let>! l =
      [%pgsql.object dbh "select * from swaps where dune_origin = $origin"] in
    List.rev_map db_swap l |> List.rev

end





module CONFIRMATIONS : sig

  val add : swap_id:int -> string -> unit Lwt.t

  val list : serial: int32 -> ( int32 * int * string ) list Lwt.t

end = struct

  let add ~swap_id pubkey =
    assert (pubkey.[1] <> 'x');
    let>>> dbh = () in
    [%pgsql dbh
        "INSERT INTO freeton_confirmations \
         (swap_id, relay_pubkey) VALUES \
         (${Int64.of_int swap_id}, $pubkey)" ]

  let list ~serial =
    let>>> dbh = () in
    let> list =
    [%pgsql dbh
        "SELECT * FROM freeton_confirmations \
         WHERE serial > $serial \
         ORDER BY serial ASC LIMIT 100
            " ]
    in
    Lwt.return @@
    List.rev_map (fun (id, swap_id, relay_pubkey) ->
       (id, Int64.to_int swap_id, relay_pubkey)
      ) (List.rev list)

end





module CONTRACTS : sig

  val get : name:string -> string option Lwt.t
  val set : name:string -> address:string -> unit Lwt.t

  val list : serial: int32 ->
    ( string * string * int32 ) list Lwt.t

end = struct

  let list ~serial =
    let>>> dbh = () in
    [%pgsql dbh
        "SELECT * FROM freeton_contracts WHERE serial > $serial \
         ORDER BY serial ASC LIMIT 100
            " ]

  let get ~name =
    assert (name.[1] <> 'x');
    let>>> dbh = () in
    let>! l =
      [%pgsql dbh
          "SELECT ADDRESS FROM freeton_contracts WHERE name = $name"] in
    match l with
    | [ contract ] -> Some contract
    | [] -> None
    | _ ->
      Printf.eprintf "Fatal error: FREETON.get_contract_address(%S)\n%!"
        name;
      assert false

  let set ~name ~address =
    assert (name.[1] <> 'x');
    let>>> dbh = () in
    [%pgsql dbh
        "INSERT INTO freeton_contracts (name, address) \
         VALUES ($name, $address)" ]

end

module CONFIG : sig

  val set : Data_types.freeton_config -> unit Lwt.t
  val get : unit -> Data_types.freeton_config option Lwt.t
  val set_last_blockid : string -> unit Lwt.t

  val update : name:string -> value:string -> unit Lwt.t
  val read : name:string -> string Lwt.t

end = struct

  let update ~name ~value =
    let>>> dbh = () in
    [%pgsql dbh
        "update freeton_state set value = $value where name = $name"
    ]

  let read ~name =
    let>>> dbh = () in
    let> res = [%pgsql dbh
        "select value from  freeton_state where name = $name"
    ] in
    match res with
    | [ v ] -> Lwt.return v
    | _ -> assert false

  let set_last_blockid blockid =
    update ~name:"last_blockid" ~value:blockid

  let set config =
    let>>> dbh = () in
    let insert name value =
      [%pgsql dbh
          "insert into freeton_state (name, value) values ($name, $value)"]
    in
    let> () = insert "network_url" config.network_url in
    let> () = insert "giver_address" config.giver_address in
    let> () = insert "root_address" config.root_address in
    let> () = insert "event_address" config.event_address in
    let> () = insert "last_blockid" config.last_blockid in
    let> () = insert "relay_pubkey" config.relay_pubkey in

    let> () = insert "monitor_database_pid" "63000" in
    let> () = insert "monitor_freeton_pid"  "63000" in
    Lwt.return_unit

  let get () =
    let>>> dbh = () in
    let get_opt name =
      let> res = [%pgsql dbh
          "select value from freeton_state where name = $name"]
      in
      match res with
      | [] -> Lwt.return_none
      | [ value ] -> Lwt.return_some value
      | _ -> assert false
    in
    let> network_url = get_opt "network_url" in
    match network_url with
    | None -> Lwt.return_none
    | Some network_url ->
      let get name =
        let> value = get_opt name in
        match value with
        | None ->
          Printf.eprintf "Error: get_config.get(%S)\n%!" name;
          assert false
        | Some value -> Lwt.return value
      in
      let> giver_address = get "giver_address" in
      let> root_address = get "root_address" in
      let> event_address = get "event_address" in
      let> last_blockid = get "last_blockid" in
      let> relay_pubkey = get "relay_pubkey" in
      Lwt.return_some {
        network_url ; giver_address ; root_address ;
        event_address ; relay_pubkey ;
        last_blockid
      }

end
