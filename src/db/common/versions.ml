let cver = ref 0
let upgrades : (int * (unit PGOCaml.t -> int -> unit)) list ref = ref []
let downgrades: (int * string list) list ref = ref []

let register_version ?version
    ?(before_upgrade = fun _ -> ())
    ?(after_upgrade = fun _ -> ())
    ~downgrade
    ~upgrade () =
  let previous_version = !cver in
  let version = match version with
    | None -> !cver + 1
    | Some v ->
      if v <= !cver then
        Format.ksprintf failwith "Registering version %d forbidden (min %d)"
          v (!cver + 1);
      v in
  (* Format.eprintf "DB: register version %d -> %d@." previous_version version; *)
  let upgrade dbh version =
    before_upgrade dbh;
    EzPG.upgrade ~dbh ~version ~downgrade upgrade;
    after_upgrade dbh;
  in
  cver := version;
  upgrades := !upgrades @ [previous_version, upgrade];
  downgrades := (version, downgrade) :: !downgrades;
  ()
;;

register_version ()
  ~upgrade:[

    {|create domain ezjsonb as jsonb|};
    {|create domain zarith as numeric|};
    {|create type op_status as enum (
      'queued',
      'pending',
      'unconfirmed',
      'confirmed',
      'error')|};

    {|create table blocks(
      hash varchar primary key,
      level int not null,
      predecessor varchar,
      block_timestamp timestamp not null,
      main boolean not null
     )|};

    {|create table block_operations(
      block varchar not null references blocks(hash),
      operation varchar not null,
      op_index smallint not null,
      success boolean not null default false,
      op_content json not null,
      primary key (block, operation, op_index))|};

    {|create table smart_contracts(
      name varchar primary key,
      address varchar not null
    )|};

    {|create table operations(
      id serial primary key,
      source varchar not null,
      op_content ezjsonb not null,
      status op_status not null default 'queued',
      op_hash varchar,
      block_hash varchar references blocks(hash),
      op_index smallint,
      confirmations int,
      op_error varchar,
      retry smallint,
      internal_index smallint
     )|};
  ]
  ~downgrade:[
    {|drop table operations|};
    {|drop table smart_contracts|};
    {|drop table block_operations|};
    {|drop table blocks|};
    {|drop type op_status|};
    {|drop domain zarith|};
    {|drop domain ezjsonb|};
  ]
;;

register_version ()
  ~upgrade:[
    {|create table blockchain_config(
      key varchar primary key,
      value_json ezjsonb not null
    )|};
  ]
  ~downgrade:[
    {|drop table blockchain_config|};
  ]
;;

register_version ()
  ~upgrade:[
    {|CREATE TYPE dune_status AS ENUM (
      'submitted',
      'confirmed',
      'can_reveal',
      'completed',
      'expired',
      'refund_asked',
      'refunded'
      )|};
    {|CREATE TYPE freeton_status AS ENUM (
     'SwapWaitingForConfirmation',
     'SwapFullyConfirmed',
     'SwapWaitingForCredit',
     'SwapCreditDenied',
     'SwapCredited',
     'SwapRevealed',
     'SwapWaitingForDepool',
     'SwapDepoolDenied',
     'SwapTransferred',
     'SwapCancelled'
      )|};
    {|CREATE TABLE swaps(
      id BIGINT PRIMARY KEY,
      dune_origin VARCHAR NOT NULL,
      dun_amount zarith NOT NULL,
      ton_amount zarith NOT NULL,
      dune_status dune_status NOT NULL,
      freeton_status freeton_status NOT NULL,
      freeton_state_count INTEGER NOT NULL,
      hashed_secret BYTEA NOT NULL,
      swap_timestamp timestamp NOT NULL,
      refund_address VARCHAR NOT NULL,
      freeton_pubkey VARCHAR NOT NULL,
      freeton_address VARCHAR NOT NULL,
      freeton_depool VARCHAR,
      confirmations INTEGER NOT NULL,
      secret BYTEA,
      logical_time BIGINT NOT NULL
    )|};
    {|CREATE TABLE freeton_confirmations (
      serial SERIAL PRIMARY KEY,
      swap_id BIGINT NOT NULL,
      relay_pubkey VARCHAR NOT NULL
    )|};
    {|CREATE TABLE swapper_info(
      id INTEGER NOT NULL PRIMARY KEY DEFAULT 0,
      value_json ezjsonb NOT NULL,
      CONSTRAINT SINGLETON CHECK (id = 0)
    )|};
    {|CREATE TABLE big_map_ids(
      name VARCHAR PRIMARY KEY,
      big_map_id INTEGER NOT NULL
    )|};
    (* name is a public key starting with '0x', the
      address then corresponds to the DuneUserSwap contract
    *)
    {|CREATE TABLE freeton_contracts(
    name VARCHAR PRIMARY KEY,
    address VARCHAR NOT NULL,
    serial SERIAL
    )|};
    (* 'last_blockid' name should return the latest block seen on FreeTON
          event_address shard,
       'network_url' name is for the URL of the network to access
    *)
    {|CREATE TABLE freeton_state(
    name VARCHAR PRIMARY KEY,
    value VARCHAR NOT NULL
    )|};
    {|CREATE TABLE logical_time(
    name VARCHAR PRIMARY KEY,
    value BIGINT NOT NULL
    )|};
  ]
  ~downgrade:[
    {|DROP TABLE big_map_ids|};
    {|DROP TABLE swapper_info|};
    {|DROP TABLE swaps|};
    {|DROP TABLE freeton_confirmations|};
    {|DROP TYPE swap_status|};
    {|DROP TABLE freeton_contracts|};
    {|DROP TABLE freeton_state|};
    {|DROP TABLE logical_time|};
  ]
;;

(*
register_version ()
  ~upgrade:[
    {|alter table operations add column internal_index smallint|};
    (* {|alter table operations add constraint opref_unique
     *   unique (block_hash, op_hash, op_index, internal_index)|}; *)
  ]
  ~downgrade:[
    (* {|alter table operations drop constraint opref_unique|}; *)
    {|alter table operations drop column internal_index|};
  ]
;;
*)
