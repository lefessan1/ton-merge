let database = Project_config.database
let unix_domain_socket_dir = "/var/run/postgresql"
let verbose_mode = ref false
let verbose_counter = ref 0

let debug fmt = Lwt_utils.debug !verbose_mode fmt

module M = Monad_lwt

module PGOCaml = struct
  include PGOCaml_generic.Make(M)

  let prepare dbh ~name ~query () =
    if !verbose_mode then
      Printf.eprintf "DB %S PREPARE %s\n%!" name query;
    prepare dbh ~name ~query ()

  let execute_rev dbh ~name ~params () =
    if !verbose_mode then begin
      incr verbose_counter;
      let counter = !verbose_counter in
      Printf.eprintf "DB x%dx begin %s\n%!" counter name;
      bind (execute_rev dbh ~name ~params ())
        (fun rows ->
           Printf.eprintf "DB x%dx end %s\n%!" counter name;
           return rows)
    end else
      execute_rev dbh ~name ~params ()

  let string_of_jsonb e = Ezjsonm.value_to_string e
  let jsonb_of_string s = Ezjsonm.value_from_string s
  let string_of_bytea b = string_of_bytea (Bytes.to_string b)
  let bytea_of_string s = bytea_of_string s |> Bytes.of_string
end

type database_handle = (string, bool) Hashtbl.t PGOCaml.t

let (>>=) = M.(>>=)
let return = M.return

let dbh_pool : database_handle Lwt_pool.t =
  let validate conn =
    PGOCaml.alive conn >>= fun is_alive ->
    debug "[Reader] Validate connection : [%b]\n%!" is_alive ;
    M.return is_alive in
  let check _conn is_ok =
    debug "[Reader] Check connection.\n%!" ;
    is_ok false in
  let dispose conn =
    debug "[Reader] Dispose connection.\n%!" ;
    PGOCaml.close conn in
  M.pool_create ~check ~validate ~dispose 20 (fun () ->
      PGOCaml.connect ?host:None ~unix_domain_socket_dir ~database ())

let (let>>>) _ f = M.pool_use dbh_pool (fun dbh -> Lwt.bind (Lwt.return dbh) f)

open Lwt_utils

let try_transaction(* _no_res *) f =
  let>>> dbh = () in
  let> () = [%pgsql dbh "BEGIN"] in
  Lwt.catch (fun () ->
      let> r = f dbh in
      let> () = [%pgsql dbh "COMMIT"] in
      Lwt.return (Ok r)
    )
    (fun exn ->
       let> () = [%pgsql dbh "ROLLBACK"] in
       Lwt.return (Error exn)
    )

let transaction f =
  let> res = try_transaction f in
  match res with
  | Ok r -> Lwt.return r
  | Error exn -> raise exn

(* let transaction f =
 *   let>>> dbh = () in
 *   let> () = [%pgsql dbh "BEGIN"] in
 *   Lwt.catch (fun () ->
 *       let> r = f dbh in
 *       let> () = match r with
 *         | Ok _ -> [%pgsql dbh "COMMIT"]
 *         | Error _ -> [%pgsql dbh "ROLLBACK"] in
 *       Lwt.return r
 *     )
 *     (fun exn ->
 *        let> () = [%pgsql dbh "ROLLBACK"] in
 *        Format.eprintf "Error in DB transaction: @[%s@]@."
 *          (Printexc.to_string exn);
 *        Error.server_error exn "Error in DB transaction, was rolled back"
 *     ) *)

type pg_error =
  | UniqueViolation of string
  | OtherPGError of string
  | OtherError of exn

let pg_error_of_exn = function
  | PGOCaml.PostgreSQL_Error (_, fields) ->
    let msg = match List.assoc_opt 'M' fields with
      | None -> "Unknown Postgres Error"
      | Some msg -> msg in
    (match List.assoc_opt 'C' fields with
     | Some "23505" (* unique_violation *) ->
       UniqueViolation msg
     | _ -> OtherPGError msg
    )
  | e -> OtherError e
