let () =
  EzPGUpdater.main Project_config.database
    ~downgrades:Db_common.VERSIONS.(!downgrades)
    ~upgrades:Db_common.VERSIONS.(!upgrades)
